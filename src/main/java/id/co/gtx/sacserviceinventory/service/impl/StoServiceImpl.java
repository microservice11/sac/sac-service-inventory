package id.co.gtx.sacserviceinventory.service.impl;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacmodules.enumz.Vendor;
import id.co.gtx.sacmodules.utils.MapperUtil;
import id.co.gtx.sacserviceinventory.entity.Sto;
import id.co.gtx.sacserviceinventory.repository.StoRepository;
import id.co.gtx.sacserviceinventory.service.StoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class StoServiceImpl extends MasterServiceImpl implements StoService {

    private final MapperUtil mapperUtil;
    private final StoRepository stoRepository;

    public StoServiceImpl(MapperUtil mapperUtil,
                          StoRepository stoRepository) {
        this.mapperUtil = mapperUtil;
        this.stoRepository = stoRepository;
    }

    @Override
    public Mono<DtoResponse<List<Sto>>> findAll() {
        DtoResponse<List<Sto>> response = new DtoResponse<>("GET ALL DATA STO", Status.FAILED, "Data STO Kosong");
        return stoRepository.findAll()
                .collectList()
                .map(stos -> {
                    if (stos.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Semua Data STO");
                        response.setData(stos);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<Sto>> findById(String id) {
        DtoResponse<Sto> response = new DtoResponse<>("GET DATA STO", Status.FAILED, "ID STO Tidak Ditemukan");
        return stoRepository.findById(id)
                .map(sto -> {
                    response.setStatus(Status.SUCCESS);
                    response.setMessage("Berhasil Menampilkan Data STO By ID");
                    response.setData(sto);
                    return response;
                })
                .switchIfEmpty(Mono.just(response));
    }

    @Override
    public Mono<DtoResponse<Sto>> findByAlias(String alias) {
        DtoResponse<Sto> response = new DtoResponse<>("GET DATA STO", Status.FAILED, "Nama Alias STO Tidak Ditemukan");
        return stoRepository.findByAlias(alias)
                .map(sto -> {
                    response.setStatus(Status.SUCCESS);
                    response.setMessage("Berhasil Menampilkan Data STO By Nama Alias");
                    response.setData(sto);
                    return response;
                })
                .switchIfEmpty(Mono.just(response));
    }

    @Override
    public Mono<DtoResponse<List<Sto>>> findByWitelId(String witelId) {
        DtoResponse<List<Sto>> response = new DtoResponse<>("GET DATA STO", Status.FAILED, "Witel Tidak Ditemukan");
        return stoRepository.findByWitelId(witelId)
                .collectList()
                .map(stos -> {
                    if (stos.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data STO By Witel");
                        response.setData(stos);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<List<Sto>>> findByWitelIdAndVendor(String witel, String vendor) {
        DtoResponse<List<Sto>> response = new DtoResponse<>("GET ALL DATA STO", Status.FAILED, "Data STO Kosong");
        return stoRepository.findByWitelIdAndVendor(witel, Enum.valueOf(Vendor.class, vendor))
                .collectList()
                .map(stos -> {
                    if (stos.size() > 0) {
                        response.setMessage("Berhasil Menampilkan Data STO By Witel");
                        response.setData(stos);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<Sto>> findByAliasAndWitelAliasAndRegionalId(String alias, String witelAlias, String regionalId) {
        DtoResponse<Sto> response = new DtoResponse<>("GET DATA STO", Status.FAILED, "Data STO Tidak Ditemukan By Alias " + alias + ", Witel Alias " + witelAlias + ", dan Regional ID " + regionalId);
        return stoRepository.findByAliasAndWitelAliasAndRegionalId(alias, witelAlias, regionalId)
                .map(sto -> {
                    response.setStatus(Status.SUCCESS);
                    response.setMessage("Berhasil Menampilkan Data STO By Alias " + alias + ", Witel Alias " + witelAlias + ", dan Regional ID " + regionalId);
                    response.setData(sto);
                    return response;
                })
                .switchIfEmpty(Mono.just(response));
    }

    @Override
    @Transactional
    public Mono<DtoResponse<Sto>> create(Sto sto) {
        DtoResponse<Sto> response = new DtoResponse<>("SAVE DATA STO", Status.FAILED, "Gagal Menyimpan Data STO");
        return findByAlias(sto.getAlias())
                .flatMap(resp -> {
                    if (resp.getStatus().equals(Status.SUCCESS)) {
                        response.setMessage("Gagal Simpan, Alias STO Sudah Ada");
                        return Mono.just(response);
                    }

                    Sto newSto = new Sto(sto);
                    return generateId()
                            .doOnNext(newSto::setId)
                            .then(stoRepository.save(newSto)
                                    .flatMap(s -> {
                                        response.setStatus(Status.SUCCESS);
                                        response.setMessage("Berhasil Save Data STO");
                                        response.setData(s);
                                        return Mono.just(response);
                                    })
                                    .doOnNext(resp1 -> auditService.auditInventory(resp1, headers))
                                    .onErrorResume(e -> {
                                        e.printStackTrace();
                                        if (!"".equals(e.getMessage()) && e.getMessage() != null) {
                                            response.setMessage(e.getMessage());
                                            if (e.getMessage().contains("violates foreign key constraint"))
                                                response.setMessage("Gagal Simpan, Witel ID Tidak Ditemukan");
                                            else if (e.getMessage().contains("duplicate key value violates unique constraint"))
                                                response.setMessage("Gagal Simpan, STO ID " + newSto.getId() + " Sudah Terdaftar");
                                        }
                                        response.setData(null);
                                        return Mono.just(response);
                                    })
                            );
                });
    }

    @Override
    @Transactional
    public Mono<DtoResponse<Sto>> update(String id, Sto sto) {
        DtoResponse<Sto> response = new DtoResponse<>("UPDATE DATA STO", Status.FAILED, "Gagal Update Data STO");
        return findById(id)
                .zipWith(findByAlias(sto.getAlias()))
                .flatMap(result -> {
                    if (result.getT1().getStatus().equals(Status.SUCCESS)) {
                        Sto oldSto = mapperUtil.mappingClass(result.getT1().getData(), Sto.class);
                        response.setOldData(oldSto);

                        if (!oldSto.getAlias().equals(sto.getAlias())) {
                            if (result.getT2().getStatus().equals(Status.SUCCESS)) {
                                response.setMessage("Gagap Update, Alias STO Sudah Ada");
                                response.setOldData(null);
                                return Mono.just(response);
                            }
                        }

                        String witelId = sto.getWitelId();
                        if (("".equals(witelId) || witelId == null) && sto.getWitel() != null) {
                            if (!"".equals(sto.getWitel().getId()) && sto.getWitel().getId() != null)
                                witelId = sto.getWitel().getId();
                        }
                        Sto newSto = result.getT1().getData();
                        newSto.setAlias(sto.getAlias());
                        newSto.setNama(sto.getNama());
                        newSto.setWitelId(witelId);
                        newSto.setUpdateAt(LocalDateTime.now());

                        return stoRepository.save(newSto)
                                .flatMap(s -> {
                                    response.setStatus(Status.SUCCESS);
                                    response.setMessage("Berhasil Update Data STO");
                                    response.setData(s);
                                    return Mono.just(response);
                                })
                                .doOnNext(resp1 -> auditService.auditInventory(resp1, headers))
                                .onErrorResume(e -> {
                                    e.printStackTrace();
                                    if (!"".equals(e.getMessage()) && e.getMessage() != null) {
                                        response.setMessage(e.getMessage());
                                        if (e.getMessage().contains("violates foreign key constraint"))
                                            response.setMessage("Gagal Simpan, Witel ID Tidak Ditemukan");
                                    }
                                    response.setData(null);
                                    response.setOldData(null);
                                    return Mono.just(response);
                                });
                    }

                    response.setMessage("Gagal Update, ID STO Tidak Ditemukan");
                    return Mono.just(response);
                });
    }

    @Override
    @Transactional
    public Mono<DtoResponse<Sto>> delete(String id) {
        DtoResponse<Sto> response = new DtoResponse<>("DELETE DATA STO", Status.FAILED, "ID STO Tidak Ditemukan");
        return findById(id)
                .flatMap(resp -> {
                    if (resp.getStatus().equals(Status.SUCCESS)) {
                        Sto sto = resp.getData();
                        return stoRepository.delete(sto)
                                .then(Mono.just(response)
                                        .map(res -> {
                                            res.setStatus(Status.SUCCESS);
                                            res.setMessage("Berhasil Hapus Data STO " + sto.getNama());
                                            res.setData(sto);
                                            return res;
                                        }))
                                .doOnNext(resp1 -> auditService.auditInventory(resp1, headers))
                                .onErrorResume(e -> {
                                    e.printStackTrace();
                                    if (!"".equals(e.getMessage()) && e.getMessage() != null) {
                                        response.setMessage(e.getMessage());
                                        if (e.getMessage().contains("violates foreign key constraint"))
                                            response.setMessage("Gagal Hapus, STO Sudah Terbinding Pada GPON");
                                    }
                                    response.setData(null);
                                    return Mono.just(response);
                                });
                    }

                    return Mono.just(response);
                });
    }

    private Mono<String> generateId() {
        return stoRepository.getMaxId()
                .map(getId -> {
                    String id = "0000000000" + getId;
                    id = id.substring(id.length() - 10);
                    return id;
                });
    }
}
