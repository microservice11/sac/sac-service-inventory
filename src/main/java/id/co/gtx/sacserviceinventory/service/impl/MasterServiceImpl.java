package id.co.gtx.sacserviceinventory.service.impl;

import id.co.gtx.sacmodules.service.HeadersService;
import id.co.gtx.sacserviceinventory.service.AuditService;
import id.co.gtx.sacserviceinventory.service.MasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class MasterServiceImpl implements MasterService {

    protected AuditService auditService;
    protected HeadersService headers;

    @Autowired
    public void setAuditService(AuditService auditService) {
        this.auditService = auditService;
    }

    @Autowired
    public void setHeaders(HeadersService headers) {
        this.headers = headers;
    }

    @Override
    public Mono<Void> setServerHttpRequest(ServerHttpRequest request) {
        headers.setRequest(request);
        return Mono.empty();
    }
}
