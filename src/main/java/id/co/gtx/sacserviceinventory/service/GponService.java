package id.co.gtx.sacserviceinventory.service;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacserviceinventory.entity.Gpon;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Mono;

import java.util.List;

public interface GponService extends MasterService {
    Mono<DtoResponse<List<Gpon>>> findAll();

    Mono<DtoResponse<List<Gpon>>> findByNms(String nmsId);

    Mono<DtoResponse<List<Gpon>>> findBySto(String stoId);

    Mono<DtoResponse<List<Gpon>>> findByStoIdAndVendor(String stoId, String vendor);

    Mono<DtoResponse<Gpon>> findById(String id);

    Mono<DtoResponse<Gpon>> findByIpAddress(String ipAddress);

    Mono<DtoResponse<Gpon>> findByHostname(String hostname);

    Mono<DtoResponse<List<String>>> findOnuTypeByVendor(String vendor);

    Mono<DtoResponse<List<Gpon>>> findByVendor(String vendor);

    Mono<DtoResponse<Gpon>> create(Gpon gpon);

    Mono<DtoResponse<List<Gpon>>> createBatch(List<Gpon> gpons);

    Mono<DtoResponse<Gpon>> update(String id, Gpon gpon);

    Mono<DtoResponse<Gpon>> delete(String id);

    ResponseEntity<Resource> downloadTemplate(String filename);
}
