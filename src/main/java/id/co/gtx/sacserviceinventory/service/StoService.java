package id.co.gtx.sacserviceinventory.service;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacserviceinventory.entity.Sto;
import reactor.core.publisher.Mono;

import java.util.List;

public interface StoService extends MasterService {
    Mono<DtoResponse<List<Sto>>> findAll();

    Mono<DtoResponse<Sto>> findById(String id);

    Mono<DtoResponse<Sto>> findByAlias(String alias);

    Mono<DtoResponse<List<Sto>>> findByWitelId(String witel);

    Mono<DtoResponse<List<Sto>>> findByWitelIdAndVendor(String witel, String vendor);

    Mono<DtoResponse<Sto>> findByAliasAndWitelAliasAndRegionalId(String alias, String witelAlias, String regionalId);

    Mono<DtoResponse<Sto>> create(Sto sto);

    Mono<DtoResponse<Sto>> update(String id, Sto sto);

    Mono<DtoResponse<Sto>> delete(String id);
}
