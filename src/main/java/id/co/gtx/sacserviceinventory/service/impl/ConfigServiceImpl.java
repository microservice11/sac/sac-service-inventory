package id.co.gtx.sacserviceinventory.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.gtx.sacmodules.dto.*;
import id.co.gtx.sacmodules.enumz.Protocol;
import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacmodules.enumz.Vendor;
import id.co.gtx.sacmodules.exception.OperationException;
import id.co.gtx.sacmodules.utils.MapperUtil;
import id.co.gtx.sacserviceinventory.service.ConfigService;
import id.co.gtx.sacserviceinventory.service.GponService;
import id.co.gtx.sacserviceinventory.service.NmsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ConfigServiceImpl extends MasterServiceImpl implements ConfigService {

    @Value("${expired-date:1900-01-01}")
    private String expiredDate;

    @Value("${max-checking-gpon}")
    private Integer maxCheckingGpon;

    private final GponService gponService;
    private final MapperUtil mapperUtil;
    private final NmsService nmsService;
    private final StreamBridge streamBridge;

    public ConfigServiceImpl(GponService gponService,
                             MapperUtil mapperUtil,
                             NmsService nmsService,
                             StreamBridge streamBridge) {
        this.gponService = gponService;
        this.mapperUtil = mapperUtil;
        this.nmsService = nmsService;
        this.streamBridge = streamBridge;
    }

    @Override
    public Mono<Void> setServerHttpRequest(ServerHttpRequest request) {
        return super.setServerHttpRequest(request)
                .then(gponService.setServerHttpRequest(request)
                        .then(nmsService.setServerHttpRequest(request))).then(Mono.empty());
    }

    @Override
    public Mono<DtoResponse<Long>> batchConfig(List<DtoConfig> dtoConfigs) {
        DtoResponse<Long> response = new DtoResponse<>("BATCH OPERATION", Status.FAILED, "GAGAL DELETE ONT MASSAL");
        return configCreate(dtoConfigs, response, "configBatch-out-0");
    }

    @Override
    public Mono<DtoResponse<Long>> checkUnreg(List<DtoConfig> configs, String vendor) {
        DtoResponse<Long> response = new DtoResponse<>("CHECK ONT", Status.FAILED, "GAGAL CEK UNREGISTER ONT");
        return checkOnt(configs, vendor, response, "checkUnreg-out-0");
    }

    @Override
    public Mono<DtoResponse<Long>> checkReg(List<DtoConfig> configs, String vendor) {
        DtoResponse<Long> response = new DtoResponse<>("CHECK ONT", Status.FAILED, "GAGAL CEK REGISTER ONT");
        return checkOnt(configs, vendor, response, "checkReg-out-0");
    }

    @Override
    public Mono<DtoResponse<Long>> checkService(List<DtoConfig> configs, String vendor) {
        DtoResponse<Long> response = new DtoResponse<>("CHECK SERVICE", Status.FAILED, "GAGAL CEK SERVICE ONT");
        return checkOnt(configs, vendor, response, "checkService-out-0");
    }

    @Override
    public Mono<DtoResponse<Long>> create(DtoConfig config) {
        DtoResponse<Long> response = new DtoResponse<>("CONFIG ONT", Status.FAILED, "GAGAL CONFIG ONT");
        return configCreate(Arrays.asList(config), response, "configCreate-out-0");
    }

    private Mono<DtoResponse<Long>> configCreate(List<DtoConfig> configs, DtoResponse<Long> response, String bindingName) {
        return getGpon(configs)
                .collectList()
                .map(dtoConfigs -> {
                    dtoConfigs.stream()
                            .filter(config -> config.getNms() != null)
                            .collect(Collectors.toList());
                    if (dtoConfigs.size() > 0) {
                        for (DtoConfig config : dtoConfigs) {
                            validation(config);
                        }

                        Date date = new Date(new Date().getTime());
                        Long proccessId = date.getTime() * 1000;

                        DtoProc<List<DtoConfig>> configProc = new DtoProc<>();
                        configProc.setProccessId(proccessId);
                        configProc.setConfigs(dtoConfigs);
                        configProc.setUsername(headers.getUsername());
                        configProc.setIpCLient(headers.getIpClient());

                        try {
                            ObjectMapper mapper = new ObjectMapper();
                            String json = mapper.writeValueAsString(configProc);
                            log.info(json);
                        } catch (JsonProcessingException e) {
                            Mono.error(new RuntimeException(e));
                        }

                        streamBridge.send(bindingName, configProc);

                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Config Sedang Berjalan, PROSES ID: " + proccessId);
                        response.setData(proccessId);
                        return response;
                    }
                    return response;
                });
    }

    //typeCheck: 1 - UNREGISTER, 2 - REGISTER, 3 - SERVICE
    private Mono<DtoResponse<Long>> checkOnt(List<DtoConfig> configs, String vendor, DtoResponse<Long> response, String bindingName) {
        return getGpon(configs)
                .collectList()
                .flatMap(dtoConfigs -> {
                    dtoConfigs.stream()
                            .filter(config -> config.getNms() != null)
                            .collect(Collectors.toList());
                    if (dtoConfigs.size() > 0) {
                        return nmsService.findMaxProtocolByVendor(vendor)
                                .flatMap(resp -> {
                                    if (resp.getStatus().equals(Status.SUCCESS)) {
                                        List<Protocol> protocols = resp.getData();
                                        if (protocols.size() == 1) {
                                            Protocol protocol = protocols.get(0);
                                            Date date = new Date(new Date().getTime());
                                            Long proccessId = date.getTime() * 1000;

                                            DtoProc<List<DtoConfig>> configProc = new DtoProc<>();
                                            configProc.setProccessId(proccessId);
                                            configProc.setProtocol(protocol);
                                            configProc.setConfigs(dtoConfigs);
                                            configProc.setUsername(headers.getUsername());
                                            configProc.setIpCLient(headers.getIpClient());


                                            try {
                                                ObjectMapper mapper = new ObjectMapper();
                                                String json = mapper.writeValueAsString(configProc);
                                                log.info(json);
                                            } catch (JsonProcessingException e) {
                                                Mono.error(new RuntimeException(e));
                                            }

                                            streamBridge.send(bindingName, configProc);

                                            response.setStatus(Status.SUCCESS);
                                            response.setMessage("Config Sedang Berjalan, PROSES ID: " + proccessId);
                                            response.setData(proccessId);
                                        } else {
                                            response.setMessage(response.getMessage() + ": Jumlah Protocol Harus 1");
                                        }
                                    } else {
                                        response.setMessage(response.getMessage() + ": " + resp.getMessage());
                                    }
                                    return Mono.just(response);
                                });
                    }
                    return Mono.just(response);
                });
    }

    private Flux<DtoConfig> getGpon(List<DtoConfig> dtoConfigs) {
        return Flux.fromIterable(dtoConfigs)
                .flatMap(config -> {
                    if (LocalDate.now().isAfter(LocalDate.parse(expiredDate, DateTimeFormatter.ofPattern("yyyy-MM-dd")))) {
                        return Mono.error(new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "License sudah expired, Hubungi Administrator", null, new DtoError("expired_license", "License sudah expired, Hubungi Administrator"))));
                    }
                    if (maxCheckingGpon == null) {
                        return Mono.error(new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Parameter Maks. Cek Gpon belum disetting")));
                    }
                    if (dtoConfigs.size() > maxCheckingGpon) {
                        return Mono.error(new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Jumlah Data Gpon yang dipilih tidak boleh lebih dari " +  maxCheckingGpon)));
                    }
                    return gponService.findByIpAddress(config.getIpGpon())
                            .map(resp -> {
                                if (resp.getStatus().equals(Status.SUCCESS)) {
                                    DtoGpon gpon = mapperUtil.mappingClass(resp.getData(), DtoGpon.class);
                                    DtoNms nms = gpon.getNms();
                                    config.setNms(nms);
                                    config.setUsername(gpon.isDefaultUserNms() ? nms.getUsername() : gpon.getUsername());
                                    config.setPassword(gpon.isDefaultUserNms() ? nms.getPassword() : gpon.getPassword());
                                }
                                return config;
                            });
                        }
                );
    }

    private void validation(DtoConfig config) {
        if ("".equals(config.getIpGpon()) || config.getIpGpon() == null) {
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "IP GPON Tidak Boleh Kosong"));
        }
        if ("".equals(config.getSlotPort()) || config.getSlotPort() == null) {
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Slot / Port Tidak Boleh Kosong"));
        }
        if ("".equals(config.getOnuId()) || config.getOnuId() == null) {
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "ONU SN/ID Tidak Boleh Kosong"));
        }
        if (config.getStatus() == null) {
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Status Konfig Tidak Boleh Kosong"));
        }
        if (config.getOpenOnt() == null) {
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Open ONT Tidak Boleh Kosong"));
        }

        if (config.getStatus() != 2) {
            if ("".equals(config.getOnuType()) || config.getOnuType() == null) {
                throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "ONU Type Tidak Boleh Kosong"));
            }
            if (config.getNms().getProtocol().equals(Protocol.SSH2)) {
                throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Konfigurasi Service Belum Support Untuk Protocol SSH2"));
            }

            if (config.getInet() != null) {
                DtoInet inet = config.getInet();
                if (inet.getStatus() == null) {
                    throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Status Internet Tidak Boleh Kosong"));
                }
                if (config.getNms().getVendor().equals(Vendor.FH) && config.getOpenOnt() == 1) {
                    for (DtoData data : inet.getData()) {
                        if (data.getMode() == null) {
                            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "WAN Mode Tidak Boleh Kosong"));
                        }
                        if (data.getConnType() == null) {
                            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Tipe Koneksi Tidak Boleh Kosong"));
                        }
                        if (inet.getStatus() == 1) {
                            if (data.getIpMode() == null) {
                                throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "IP Mode Tidak Boleh Kosong"));
                            }
                            if (data.getQos() == null) {
                                throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "QOS Tidak Boleh Kosong"));
                            }
                            if (data.getLan().length == 0) {
                                throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "List LAN Tidak Boleh Kosong"));
                            }
                            if (data.getSsid().length == 0) {
                                throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "List SSID Tidak Boleh Kosong"));
                            }
                        }
                    }
                }
                if (config.getNms().getVendor().equals(Vendor.ZTE) && (inet.getVport() == null || inet.getData().size() == 0)) {
                    if (inet.getVport() == null) {
                        throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "VPORT Internet Tidak Boleh Kosong"));
                    }
                    if (inet.getData().size() == 0) {
                        throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "DATA Internet Tidak Boleh Kosong"));
                    }
                }
                for (DtoData data : inet.getData()) {
                    if (data.getVlan() == null) {
                        throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "VLAN Internet Tidak Boleh Kosong"));
                    }
                    if (inet.getStatus() == 1) {
                        if (("".equals(data.getAcc()) || data.getAcc() == null) && config.getOpenOnt() == 1) {
                            if (config.getNms().getVendor().equals(Vendor.FH)) {
                                if (data.getConnType() == 2 && data.getIpMode() == 3) {
                                    throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Akun Internet Tidak Boleh Kosong"));
                                }
                            } else {
                                throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Akun Internet Tidak Boleh Kosong"));
                            }
                        }
                        if (("".equals(data.getPwd()) || data.getPwd() == null) && config.getOpenOnt() == 1) {
                            if (config.getNms().getVendor().equals(Vendor.FH)) {
                                if (data.getConnType() == 2 && data.getIpMode() == 3) {
                                    throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Password Internet Tidak Boleh Kosong"));
                                }
                            } else {
                                throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Password Internet Tidak Boleh Kosong"));
                            }
                        }

                        if ("".equals(data.getBwUp()) || data.getBwUp() == null) {
                            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Upload Internet Tidak Boleh Kosong"));
                        }
                        if ("".equals(data.getBwDown()) || data.getBwDown() == null) {
                            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Download Internet Tidak Boleh Kosong"));
                        }
                    }
                }
            }

            if (config.getVoip() != null) {
                DtoVoip voip = config.getVoip();
                if (voip.getStatus() == null) {
                    throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Status Voip Tidak Boleh Kosong"));
                }
                if (voip.getData().size() > 0) {
                    for (DtoData data : voip.getData()) {
                        if (config.getOpenOnt() == 1) {
                            if (data.getPots() == null) {
                                throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Potst Voip Tidak Boleh Kosong"));
                            }
                            if (voip.getStatus() == 1) {
                                if ("".equals(data.getAcc()) || data.getAcc() == null) {
                                    throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Akun Voip Tidak Boleh Kosong"));
                                }
                                if ("".equals(data.getPwd()) || data.getPwd() == null) {
                                    throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Password Voip Tidak Boleh Kosong"));
                                }
                            }
                        }
                    }
                } else {
                    if (config.getOpenOnt() == 1) {
                        throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "DATA Voip Tidak Boleh Kosong"));
                    }
                }
                if (voip.getVlan() == null) {
                    throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "VLAN Voip Tidak Boleh Kosong"));
                }
                if (voip.getVport() == null && config.getNms().getVendor().equals(Vendor.ZTE)) {
                    throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "VPORT Voip Tidak Boleh Kosong"));
                }
            }

            if (config.getIptv() != null) {
                DtoIptv iptv = config.getIptv();
                if (iptv.getStatus() == null) {
                    throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Status IPTV Tidak Boleh Kosong"));
                }
                if (iptv.getStatus() != 0 && iptv.getLan().length == 0 && config.getOpenOnt() == 1) {
                    throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "LAN IPTV Tidak Boleh Kosong"));
                }
            }

            if (config.getOther() != null) {
                DtoOther other = config.getOther();
                if (other.getStatus() == null) {
                    throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Status OTHER Tidak Boleh Kosong"));
                }
                if (other.getData().size() > 0) {
                    for (DtoData data : other.getData()) {
                        if (("".equals(data.getName()) || data.getName() == null) && config.getNms().getVendor().equals(Vendor.ZTE)) {
                            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Nama OTHER Tidak Boleh Kosong"));
                        }
                        if (data.getVlan() == null) {
                            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "VLAN OTHER Tidak Boleh Kosong"));
                        }
                        if (other.getStatus() == 1) {
                            if ("".equals(data.getBwUp()) || data.getBwUp() == null) {
                                throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Upload OTHER Tidak Boleh Kosong"));
                            }
                            if ("".equals(data.getBwDown()) || data.getBwDown() == null) {
                                throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Download OTHER Tidak Boleh Kosong"));
                            }
                        }
                        if (config.getOpenOnt() == 1) {
                            if (data.getSvlan() == null) {
                                throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "USER VLAN OTHER Tidak Boleh Kosong"));
                            }

                            if (data.getLan().length == 0) {
                                throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "LAN OTHER Tidak Boleh Kosong"));
                            }
                        }
                    }
                } else {
                    if (config.getOpenOnt() == 1) {
                        throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Data OTHER Tidak Boleh Kosong"));
                    }
                }
            }
        }
    }
}
