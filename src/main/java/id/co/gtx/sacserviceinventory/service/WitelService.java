package id.co.gtx.sacserviceinventory.service;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacmodules.enumz.Vendor;
import id.co.gtx.sacserviceinventory.entity.Witel;
import reactor.core.publisher.Mono;

import java.util.List;

public interface WitelService extends MasterService {
    Mono<DtoResponse<List<Witel>>> findAll();

    Mono<DtoResponse<Witel>> findById(String id);

    Mono<DtoResponse<List<Witel>>> findByIdInAndRegionalIdAndVendor(List<String> id, String regionalId, Vendor vendor);

    Mono<DtoResponse<Witel>> findByAlias(String alias);

    Mono<DtoResponse<List<Witel>>> findByRegionalId(String regionalId);

    Mono<DtoResponse<List<Witel>>> findByRegionalIdIn(String[] regionalId);

    Mono<DtoResponse<List<Witel>>> findByRegionalAndVendor(String regionalId, String vendor);

    Mono<DtoResponse<Witel>> create(Witel witel);

    Mono<DtoResponse<Witel>> update(String id, Witel witel);

    Mono<DtoResponse<Witel>> delete(String id);
}
