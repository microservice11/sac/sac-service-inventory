package id.co.gtx.sacserviceinventory.service.impl;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacmodules.enumz.Protocol;
import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacmodules.enumz.Vendor;
import id.co.gtx.sacmodules.utils.MapperUtil;
import id.co.gtx.sacserviceinventory.entity.Nms;
import id.co.gtx.sacserviceinventory.repository.NmsRepository;
import id.co.gtx.sacserviceinventory.repository.query.NmsQueryRepository;
import id.co.gtx.sacserviceinventory.service.NmsService;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

@Service
public class NmsServiceImpl extends MasterServiceImpl implements NmsService {

    private final MapperUtil mapperUtil;
    private final NmsRepository nmsRepository;
    private final NmsQueryRepository nmsQueryRepository;

    public NmsServiceImpl(MapperUtil mapperUtil,
                          NmsRepository nmsRepository,
                          NmsQueryRepository nmsQueryRepository) {
        this.mapperUtil = mapperUtil;
        this.nmsRepository = nmsRepository;
        this.nmsQueryRepository = nmsQueryRepository;
    }

    @Override
    public Mono<DtoResponse<List<Nms>>> findAll() {
        DtoResponse<List<Nms>> response = new DtoResponse<>("GET ALL DATA NMS " + Nms.class.getName(), Status.FAILED, "Data NMS Kosong");
        return nmsRepository.findAll(Sort.by("nama").and(Sort.by("ipServer")).and(Sort.by("vendor")))
                .collectList()
                .map(nms -> {
                    if (nms.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Semua Data NMS");
                        response.setData(nms);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<Nms>> findById(String id) {
        DtoResponse<Nms> response = new DtoResponse<>("GET DATA NMS " + Nms.class.getName(), Status.FAILED, "ID NMS Tidak Ditemukan");
        return nmsRepository.findById(id)
                .map(nms -> {
                    response.setStatus(Status.SUCCESS);
                    response.setMessage("Berhasil Menampilkan Data NMS By ID");
                    response.setData(nms);
                    return response;
                })
                .switchIfEmpty(Mono.just(response));
    }

    @Override
    public Mono<DtoResponse<Nms>> findByIpServer(String ipServer) {
        DtoResponse<Nms> response = new DtoResponse<>("GET DATA NMS " + Nms.class.getName(), Status.FAILED, "Ip Address NMS Tidak Ditemukan");
        return nmsRepository.findByIpServer(ipServer)
                .map(nms -> {
                    response.setStatus(Status.SUCCESS);
                    response.setMessage("Berhasil Menampilkan Data NMS By Ip Address");
                    response.setData(nms);
                    return response;
                })
                .switchIfEmpty(Mono.just(response));
    }

    @Override
    public Mono<DtoResponse<Nms>> findByNama(String nama) {
        DtoResponse<Nms> response = new DtoResponse<>("GET DATA NMS " + Nms.class.getName(), Status.FAILED, "Hostname NMS Tidak Ditemukan");
        return nmsRepository.findByNama(nama)
                .map(nms -> {
                    response.setStatus(Status.SUCCESS);
                    response.setMessage("Berhasil Menampilkan Data NMS By Hostname");
                    response.setData(nms);
                    return response;
                })
                .switchIfEmpty(Mono.just(response));
    }

    @Override
    public Mono<DtoResponse<List<Vendor>>> getVendor() {
        DtoResponse<List<Vendor>> response = new DtoResponse<>("GET DATA NMS " + Vendor.class.getName(), Status.FAILED, "Data Vendor Belum Ada");
        return nmsQueryRepository.findDistinctVendor()
                .collectList()
                .map(vendors -> {
                    if (vendors.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data Vendor");
                        response.setData(vendors);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<List<Protocol>>> getProtocol() {
        List<Protocol> protocols = Arrays.asList(Protocol.values());
        DtoResponse<List<Protocol>> response = new DtoResponse<>("GET DATA NMS " + Protocol.class.getName(), Status.FAILED, "Protocol NMS Belum Ada");
        if (protocols.size() > 0) {
            response.setStatus(Status.SUCCESS);
            response.setMessage("Berhasil Menampilkan Data Protocol");
            response.setData(protocols);
        }
        return Mono.just(response);
    }

    @Override
    public Mono<DtoResponse<List<Protocol>>> findMaxProtocolByVendor(String vendor) {
        DtoResponse<List<Protocol>> response = new DtoResponse<>("GET DATA NMS " + Nms.class.getName(), Status.FAILED, "Vendor NMS Tidak Ditemukan");
        return nmsQueryRepository.findDistinctProtocolByVendor(Enum.valueOf(Vendor.class, vendor))
                .collectList()
                .map(protocols -> {
                    if (protocols.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data Protocol By Vendor");
                        response.setData(protocols);
                    }
                    return response;
                });
    }

    @Override
    @Transactional
    public Mono<DtoResponse<Nms>> create(Nms nms) {
        DtoResponse<Nms> response = new DtoResponse<>("SAVE DATA NMS " + Nms.class.getName(), Status.FAILED, "Gagal Menyimpan Data NMS");
        return findByIpServer(nms.getIpServer())
                .zipWith(findByNama(nms.getNama()))
                .flatMap(result -> {
                    DtoResponse<Nms> resp = result.getT1();
                    if (resp.getStatus().equals(Status.SUCCESS)) {
                        response.setMessage("Ip Address NMS Sudah Ada");
                        return Mono.just(response);
                    }

                    if (result.getT2().getStatus().equals(Status.SUCCESS)) {
                        response.setMessage("Hostname NMS Sudah Ada");
                        return Mono.just(response);
                    }

                    Nms newNms = new Nms(nms);
                    return generateId()
                            .doOnNext(newNms::setId)
                            .then(nmsRepository.save(newNms)
                                    .flatMap(n -> {
                                        response.setStatus(Status.SUCCESS);
                                        response.setMessage("Berhasil Menyimpan Data NMS");
                                        response.setData(n);
                                        return Mono.just(response);
                                    })
                                    .doOnNext(resp1 -> auditService.auditInventory(resp1, headers))
                                    .onErrorResume(e -> {
                                        e.printStackTrace();
                                        if (!"".equals(e.getMessage()) && e.getMessage() != null) {
                                            response.setMessage(e.getMessage());
                                            if (e.getMessage().contains("duplicate key value violates unique constraint"))
                                                response.setMessage("Gagal Simpan, STO ID " + newNms.getId() + " Sudah Terdaftar");
                                        }
                                        response.setData(null);
                                        return Mono.just(response);
                                    })
                            );
                });
    }

    @Override
    @Transactional
    public Mono<DtoResponse<Nms>> update(String id, Nms nms) {
        DtoResponse<Nms> response = new DtoResponse<>("UPDATE DATA NMS " + Nms.class.getName(), Status.FAILED, "Gagal Update Data NMS");
        return findById(id)
                .zipWith(findByIpServer(nms.getIpServer()))
                .zipWith(findByNama(nms.getNama()))
                .flatMap(result -> {
                    DtoResponse<Nms> resp = result.getT1().getT1();
                    if (resp.getStatus().equals(Status.SUCCESS)) {
                        Nms oldNms = mapperUtil.mappingClass(resp.getData(), Nms.class);
                        response.setOldData(oldNms);

                        if (!oldNms.getIpServer().equals(nms.getIpServer())) {
                            if (result.getT1().getT2().getStatus().equals(Status.SUCCESS)) {
                                response.setMessage("Ip Address NMS Sudah Ada");
                                response.setOldData(null);
                                return Mono.just(response);
                            }
                        }

                        if (!oldNms.getNama().equals(nms.getNama())) {
                            if (result.getT2().getStatus().equals(Status.SUCCESS)) {
                                response.setMessage("Hostname NMS Sudah Ada");
                                response.setOldData(null);
                                return Mono.just(response);
                            }
                        }

                        Nms newNms = resp.getData();
                        newNms.setIpServer(nms.getIpServer());
                        newNms.setPortTl1(nms.getPortTl1());
                        newNms.setNama(nms.getNama());
                        newNms.setVendor(nms.getVendor());
                        newNms.setProtocol(nms.getProtocol());
                        if (!"".equals(nms.getUsername()) && nms.getUsername() != null)
                            newNms.setUsername(Base64.getEncoder().encodeToString(nms.getUsername().getBytes(StandardCharsets.UTF_8)));
                        if (!"".equals(nms.getPassword()) && nms.getPassword() != null)
                            newNms.setPassword(Base64.getEncoder().encodeToString(nms.getPassword().getBytes(StandardCharsets.UTF_8)));
                        newNms.setUpdateAt(LocalDateTime.now());

                        return nmsRepository.save(newNms)
                                .flatMap(n -> {
                                    response.setStatus(Status.SUCCESS);
                                    response.setMessage("Berhasil Update Data NMS");
                                    response.setData(n);
                                    return Mono.just(response);
                                })
                                .doOnNext(resp1 -> auditService.auditInventory(resp1, headers))
                                .onErrorResume(e -> {
                                    e.printStackTrace();
                                    if (!"".equals(e.getMessage()) && e.getMessage() != null)
                                        response.setMessage(e.getMessage());
                                    response.setData(null);
                                    response.setOldData(null);
                                    return Mono.just(response);
                                });
                    }

                    response.setMessage("Gagal Update, ID NMS Tidak Ditemukan");
                    return Mono.just(response);
                });
    }

    @Override
    @Transactional
    public Mono<DtoResponse<Nms>> delete(String id) {
        DtoResponse<Nms> response = new DtoResponse<>("DELETE DATA NMS " + Nms.class.getName(), Status.FAILED, "ID NMS Tidak Ditemukan");
        return findById(id)
                .flatMap(resp -> {
                    if (resp.getStatus().equals(Status.SUCCESS)) {
                        Nms nms = resp.getData();
                        return nmsRepository.delete(nms)
                                .then(Mono.just(response)
                                        .map(res -> {
                                            response.setStatus(Status.SUCCESS);
                                            response.setMessage("Berhasil Hapus Data NMS " + nms.getNama());
                                            response.setData(nms);
                                            return response;
                                        }))
                                .doOnNext(resp1 -> auditService.auditInventory(resp1, headers))
                                .onErrorResume(e -> {
                                    e.printStackTrace();
                                    if (!"".equals(e.getMessage()) && e.getMessage() != null) {
                                        response.setMessage(e.getMessage());
                                        if (e.getMessage().contains("violates foreign key constraint"))
                                            response.setMessage("Gagal Hapus, NMS Sudah Terbinding Pada GPON");
                                    }
                                    response.setData(null);
                                    return Mono.just(response);
                                });
                    }

                    return Mono.just(response);
                });
    }

    private Mono<String> generateId() {
        return nmsRepository.getMaxId()
                .map(getId -> {
                    String id = "0000000000" + getId;
                    id = id.substring(id.length() - 10);
                    return id;
                });
    }
}
