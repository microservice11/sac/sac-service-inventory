package id.co.gtx.sacserviceinventory.service;

import id.co.gtx.sacmodules.dto.DtoConfig;
import id.co.gtx.sacmodules.dto.DtoResponse;
import reactor.core.publisher.Mono;

import java.util.List;

public interface ConfigService extends MasterService {
    Mono<DtoResponse<Long>> batchConfig(List<DtoConfig> configs);

    Mono<DtoResponse<Long>> checkUnreg(List<DtoConfig> configs, String vendor);

    Mono<DtoResponse<Long>> checkReg(List<DtoConfig> configs, String vendor);

    Mono<DtoResponse<Long>> checkService(List<DtoConfig> configs, String vendor);

    Mono<DtoResponse<Long>> create(DtoConfig config);
}
