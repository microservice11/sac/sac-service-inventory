package id.co.gtx.sacserviceinventory.service;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacmodules.enumz.Protocol;
import id.co.gtx.sacmodules.enumz.Vendor;
import id.co.gtx.sacserviceinventory.entity.Nms;
import reactor.core.publisher.Mono;

import java.util.List;

public interface NmsService extends MasterService {
    Mono<DtoResponse<List<Nms>>> findAll();

    Mono<DtoResponse<Nms>> findById(String id);

    Mono<DtoResponse<Nms>> findByIpServer(String ipServer);

    Mono<DtoResponse<Nms>> findByNama(String nama);

    Mono<DtoResponse<List<Vendor>>> getVendor();

    Mono<DtoResponse<List<Protocol>>> getProtocol();

    Mono<DtoResponse<List<Protocol>>> findMaxProtocolByVendor(String vendor);

    Mono<DtoResponse<Nms>> create(Nms nms);

    Mono<DtoResponse<Nms>> update(String id, Nms nms);

    Mono<DtoResponse<Nms>> delete(String id);
}
