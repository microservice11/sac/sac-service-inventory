package id.co.gtx.sacserviceinventory.service.impl;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacmodules.enumz.Vendor;
import id.co.gtx.sacmodules.utils.MapperUtil;
import id.co.gtx.sacserviceinventory.entity.Witel;
import id.co.gtx.sacserviceinventory.repository.WitelRepository;
import id.co.gtx.sacserviceinventory.repository.query.WitelQueryRepository;
import id.co.gtx.sacserviceinventory.service.WitelService;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class WitelServiceImpl extends MasterServiceImpl implements WitelService {

    private final MapperUtil mapperUtil;
    private final WitelRepository witelRepository;
    private final WitelQueryRepository witelQueryRepository;
    private final StreamBridge streamBridge;

    public WitelServiceImpl(MapperUtil mapperUtil,
                            WitelRepository witelRepository,
                            WitelQueryRepository witelQueryRepository,
                            StreamBridge streamBridge) {
        this.mapperUtil = mapperUtil;
        this.witelRepository = witelRepository;
        this.witelQueryRepository = witelQueryRepository;
        this.streamBridge = streamBridge;
    }

    @Override
    public Mono<DtoResponse<List<Witel>>> findAll() {
        DtoResponse<List<Witel>> response = new DtoResponse<>("GET ALL DATA WITEL", Status.FAILED, "Data Witel Kosong");
        return witelRepository.findAll()
                .collectList()
                .map(witels -> {
                    if (witels.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Semua Data Witel");
                        response.setData(witels);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<Witel>> findById(String id) {
        DtoResponse<Witel> response = new DtoResponse<>("GET DATA WITEL", Status.FAILED, "ID Witel Tidak Ditemukan");
        return witelRepository.findById(id)
                .map(witel -> {
                    response.setStatus(Status.SUCCESS);
                    response.setMessage("Berhasil Menampilkan Data Witel By ID");
                    response.setData(witel);
                    return response;
                })
                .switchIfEmpty(Mono.just(response));
    }

    @Override
    public Mono<DtoResponse<List<Witel>>> findByIdInAndRegionalIdAndVendor(List<String> id, String regionalId, Vendor vendor) {
        DtoResponse<List<Witel>> response = new DtoResponse<>("GET DATA WITEL", Status.FAILED, "Witel Tidak Ditemukan By ID: " + id + ", Regional ID: " + regionalId + ", Vendor: " + vendor.getValue());
        return witelQueryRepository.findByIdInAndRegionalIdAndVendor(id, regionalId, vendor)
                .collectList()
                .map(witels -> {
                    if (witels.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data Witel By ID: " + id + ", Regional ID: " + regionalId + ", Vendor: " + vendor.getValue());
                        response.setData(witels);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<Witel>> findByAlias(String alias) {
        DtoResponse<Witel> response = new DtoResponse<>("GET DATA WITEL", Status.FAILED, "Alias Witel Tidak Ditemukan");
        return witelRepository.findByAlias(alias)
                .map(witel -> {
                    response.setStatus(Status.SUCCESS);
                    response.setMessage("Berhasil Menampilkan Data Witel By Alias");
                    response.setData(witel);
                    return response;
                })
                .switchIfEmpty(Mono.just(response));
    }

    @Override
    public Mono<DtoResponse<List<Witel>>> findByRegionalId(String regionalId) {
        DtoResponse<List<Witel>> response = new DtoResponse<>("GET DATA WITEL", Status.FAILED, "Data Witel Tidak Ditemukan By Regional ID");
        return witelRepository.findByRegionalIdOrderByAliasAsc(regionalId)
                .collectList()
                .map(witels -> {
                    if (witels.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data Witel By Regional ID");
                        response.setData(witels);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<List<Witel>>> findByRegionalIdIn(String[] regionalId) {
        DtoResponse<List<Witel>> response = new DtoResponse<>("GET DATA WITEL", Status.FAILED, "Data Witel Tidak Ditemukan By Regional");
        return witelRepository.findByRegionalIdInOrderByAliasAsc(Arrays.asList(regionalId))
                .collectList()
                .map(witels -> {
                    if (witels.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data Witel By Regional");
                        response.setData(witels);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<List<Witel>>> findByRegionalAndVendor(String regionalId, String vendor) {
        Flux<Witel> witelFlux;
        if (headers.getWitel().size() > 0) {
            witelFlux = witelRepository.findByIdInAndRegionalAndVendor(headers.getWitel(), regionalId, Enum.valueOf(Vendor.class, vendor));
        } else {
            witelFlux = witelRepository.findByRegionalAndVendor(regionalId, Enum.valueOf(Vendor.class, vendor));
        }
        DtoResponse<List<Witel>> response = new DtoResponse<>("GET DATA WITEL", Status.FAILED, "Data Witel Tidak Ditemukan By Regional ID dan Vendor");
        return witelFlux
                .collectList()
                .map(witels -> {
                    if (witels.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data Witel By Regional ID dan Vendor");
                        response.setData(witels);
                    }
                    return response;
                });
    }

    @Override
    @Transactional
    public Mono<DtoResponse<Witel>> create(Witel witel) {
        DtoResponse<Witel> response = new DtoResponse<>("SAVE DATA WITEL", Status.FAILED, "Gagal Menyimpan Data Witel");
        return findByAlias(witel.getAlias())
                .flatMap(resp -> {
                    if (resp.getStatus().equals(Status.SUCCESS)) {
                        response.setMessage("Gagal Simpan, Nama Alias Witel Sudah Ada");
                        return Mono.just(response);
                    }

                    Witel newWitel = new Witel(witel);
                    return generateId()
                            .doOnNext(newWitel::setId)
                            .then(witelRepository.save(newWitel)
                                    .flatMap(w -> {
                                        response.setStatus(Status.SUCCESS);
                                        response.setMessage("Berhasil Save Data Witel");
                                        response.setData(w);
                                        return Mono.just(response);
                                    })
                                    .doOnNext(resp1 -> auditService.auditInventory(resp1, headers))
                                    .onErrorResume(e -> {
                                        e.printStackTrace();
                                        if (!"".equals(e.getMessage()) && e.getMessage() != null) {
                                            response.setMessage(e.getMessage());
                                            if (e.getMessage().contains("violates foreign key constraint"))
                                                response.setMessage("Gagal Simpan, Regional ID Tidak Ditemukan");
                                            else if (e.getMessage().contains("duplicate key value violates unique constraint"))
                                                response.setMessage("Gagal Simpan, Witel ID " + newWitel.getId() + " Sudah Terdaftar");
                                        }
                                        response.setData(null);
                                        return Mono.just(response);
                                    })
                            );
                });
    }

    @Override
    @Transactional
    public Mono<DtoResponse<Witel>> update(String id, Witel witel) {
        DtoResponse<Witel> response = new DtoResponse<>("UPDATE DATA WITEL", Status.FAILED, "Gagal Update Data Witel");
        return findById(id)
                .zipWith(findByAlias(witel.getAlias()))
                .flatMap(result -> {
                    if (result.getT1().getStatus().equals(Status.SUCCESS)) {
                        Witel oldWitel = mapperUtil.mappingClass(result.getT1().getData(), Witel.class);
                        oldWitel.setRegional(null);
                        response.setOldData(oldWitel);

                        if (!oldWitel.getAlias().equals(witel.getAlias())) {
                            if (result.getT2().getStatus().equals(Status.SUCCESS)) {
                                response.setMessage("Gagal Update, Alias Witel Sudah Ada");
                                response.setOldData(null);
                                return Mono.just(response);
                            }
                        }

                        String regionalId = witel.getRegionalId();
                        if (regionalId == null && witel.getRegional() != null) {
                            if ("".equals(witel.getRegional().getId()) && witel.getRegional().getId() != null)
                                regionalId = witel.getRegional().getId();
                        }
                        Witel newWitel = result.getT1().getData();
                        newWitel.setAlias(witel.getAlias());
                        newWitel.setNama(witel.getNama());
                        newWitel.setRegionalId(regionalId);
                        newWitel.setUpdateAt(LocalDateTime.now());

                        return witelRepository.save(newWitel)
                                .flatMap(w -> {
                                    w.setRegional(null);
                                    response.setStatus(Status.SUCCESS);
                                    response.setMessage("Berhasil Update Data Witel");
                                    response.setData(w);
                                    return Mono.just(response);
                                })
                                .doOnNext(resp -> auditService.auditInventory(resp, headers))
                                .onErrorResume(e -> {
                                    e.printStackTrace();
                                    if (!"".equals(e.getMessage()) && e.getMessage() != null) {
                                        response.setMessage(e.getMessage());
                                        if (e.getMessage().contains("violates foreign key constraint"))
                                            response.setMessage("Gagal Simpan, Regional ID Tidak Ditemukan");
                                    }
                                    response.setData(null);
                                    response.setOldData(null);
                                    return Mono.just(response);
                                });
                    }

                    response.setMessage("Gagal Update, ID Witel Tidak Ditemukan");
                    return Mono.just(response);
                });
    }

    @Override
    @Transactional
    public Mono<DtoResponse<Witel>> delete(String id) {
        DtoResponse<Witel> response = new DtoResponse<>("DELETE DATA WITEL", Status.FAILED, "ID Witel Tidak Ditemukan");
        return findById(id)
                .flatMap(resp -> {
                    if (resp.getStatus().equals(Status.SUCCESS)) {
                        Witel witel = resp.getData();
                        witel.setRegional(null);
                        return witelRepository.delete(witel)
                                .then(Mono.just(response)
                                        .map(res -> {
                                            res.setStatus(Status.SUCCESS);
                                            res.setMessage("Berhasil Hapus Data Witel " + witel.getNama());
                                            res.setData(witel);

                                            Map<String, String> map = new HashMap<>();
                                            map.put("id", witel.getId());
                                            streamBridge.send("removeData-out-0", map);

                                            return res;
                                        }))
                                .doOnNext(resp1 -> auditService.auditInventory(resp1, headers))
                                .onErrorResume(e -> {
                                    e.printStackTrace();
                                    if (!"".equals(e.getMessage()) && e.getMessage() != null) {
                                        response.setMessage(e.getMessage());
                                        if (e.getMessage().contains("violates foreign key constraint"))
                                            response.setMessage("Gagal Hapus, Witel Sudah Terbinding Pada STO");
                                    }
                                    response.setData(null);
                                    return Mono.just(response);
                                });
                    }

                    return Mono.just(response);
                });
    }

    private Mono<String> generateId() {
        return witelRepository.getMaxId()
                .map(getId -> {
                    String id = "0000000000" + getId;
                    id = id.substring(id.length() - 10);
                    return id;
                });
    }
}
