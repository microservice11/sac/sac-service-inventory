package id.co.gtx.sacserviceinventory.service.impl;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacmodules.dto.StatusCode;
import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacmodules.enumz.Vendor;
import id.co.gtx.sacmodules.exception.OperationException;
import id.co.gtx.sacmodules.utils.MapperUtil;
import id.co.gtx.sacserviceinventory.config.properties.ConfigProperties;
import id.co.gtx.sacserviceinventory.entity.Gpon;
import id.co.gtx.sacserviceinventory.repository.GponRepository;
import id.co.gtx.sacserviceinventory.service.GponService;
import id.co.gtx.sacserviceinventory.service.NmsService;
import id.co.gtx.sacserviceinventory.service.RegionalService;
import id.co.gtx.sacserviceinventory.service.StoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class GponServiceImpl extends MasterServiceImpl implements GponService {

    private final ConfigProperties configProperties;
    private final GponRepository gponRepository;
    private final MapperUtil mapperUtil;
    private final NmsService nmsService;
    private final StoService stoService;
    private final RegionalService regionalService;
    private final FileStorageServiceImpl fileStorageService;

    public GponServiceImpl(ConfigProperties configProperties,
                           GponRepository gponRepository,
                           MapperUtil mapperUtil,
                           NmsService nmsService,
                           StoService stoService,
                           RegionalService regionalService,
                           FileStorageServiceImpl fileStorageService) {
        this.configProperties = configProperties;
        this.gponRepository = gponRepository;
        this.mapperUtil = mapperUtil;
        this.nmsService = nmsService;
        this.stoService = stoService;
        this.regionalService = regionalService;
        this.fileStorageService = fileStorageService;
    }

    @Override
    public Mono<DtoResponse<List<Gpon>>> findAll() {
        DtoResponse<List<Gpon>> response = new DtoResponse("GET ALL DATA GPON", Status.SUCCESS, "Data GPON Kosong");
        return gponRepository.findAll()
                .collectList()
                .map(gpons -> {
                    if (gpons.size() > 0) {
                        response.setMessage("Berhasil Menampilkan Semua Data GPON");
                        response.setData(gpons);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<List<Gpon>>> findByNms(String nmsId) {
        DtoResponse<List<Gpon>> response = new DtoResponse("GET DATA GPON", Status.FAILED, "Data GPON Tidak Ditemukan By NMS ID " + nmsId);
        return gponRepository.findByStoId(nmsId)
                .collectList()
                .map(gpons -> {
                    if (gpons.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data GPON By NMS ID " + nmsId);
                        response.setData(gpons);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<List<Gpon>>> findBySto(String stoId) {
        DtoResponse<List<Gpon>> response = new DtoResponse("GET DATA GPON", Status.FAILED, "Data GPON Tidak Ditemukan By STO ID " + stoId);
        return gponRepository.findByStoId(stoId)
                .collectList()
                .map(gpons -> {
                    if (gpons.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data GPON By STO ID " + stoId);
                        response.setData(gpons);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<List<Gpon>>> findByStoIdAndVendor(String stoId, String vendor) {
        DtoResponse<List<Gpon>> response = new DtoResponse("GET DATA GPON", Status.FAILED, "Data GPON Tidak Ditemukan By STO ID " + stoId + " Dan Vendor " + vendor);
        return gponRepository.findByStoAndVendor(stoId, Enum.valueOf(Vendor.class, vendor))
                .collectList()
                .map(gpons -> {
                    if (gpons.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data GPON By STO ID " + stoId + " Dan Vendor " + vendor);
                        response.setData(gpons);
                        return response;
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<Gpon>> findById(String id) {
        DtoResponse<Gpon> response = new DtoResponse<>("GET DATA GPON", Status.FAILED, "ID GPON Tidak Ditemukan");
        return gponRepository.findById(id)
                .map(gpon -> {
                    response.setStatus(Status.SUCCESS);
                    response.setMessage("Berhasil Menampilkan Data GPON By ID");
                    response.setData(gpon);
                    return response;
                })
                .switchIfEmpty(Mono.just(response));
    }

    @Override
    public Mono<DtoResponse<Gpon>> findByIpAddress(String ipAddress) {
        DtoResponse<Gpon> response = new DtoResponse<>("GET DATA GPON", Status.FAILED, "Ip Address GPON Tidak Ditemukan");
        if (headers.getGroupAll().size() > 0) {
            return gponRepository.findByIpAddressAndRegionalIdIn(ipAddress, headers.getRegional())
                    .map(gpon -> {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data GPON By Ip Address Dan Regional");
                        response.setData(gpon);
                        return response;
                    })
                    .switchIfEmpty(gponRepository.findByIpAddressAndWitelIdIn(ipAddress, headers.getWitel())
                            .map(gpon -> {
                                response.setStatus(Status.SUCCESS);
                                response.setMessage("Berhasil Menampilkan Data GPON By Ip Address Dan Witel");
                                response.setData(gpon);
                                return response;
                            })
                            .switchIfEmpty(Mono.just(response))
                    );
        }

        return gponRepository.findByIpAddress(ipAddress)
                .map(gpon -> {
                    response.setStatus(Status.SUCCESS);
                    response.setMessage("Berhasil Menampilkan Data GPON By Ip Address");
                    response.setData(gpon);
                    return response;
                })
                .switchIfEmpty(Mono.just(response));
    }

    @Override
    public Mono<DtoResponse<Gpon>> findByHostname(String hostname) {
        DtoResponse<Gpon> response = new DtoResponse<>("GET DATA GPON", Status.FAILED, "Hostname GPON Tidak Ditemukan");
        return gponRepository.findByHostname(hostname)
                .map(gpon -> {
                    response.setStatus(Status.SUCCESS);
                    response.setMessage("Berhasil Menampilkan Data GPON By Hostname");
                    response.setData(gpon);
                    return response;
                })
                .switchIfEmpty(Mono.just(response));
    }

    @Override
    public Mono<DtoResponse<List<String>>> findOnuTypeByVendor(String vendor) {
        if (configProperties.getOnuType() == null) {
            return Mono.error(new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "ONU Type Tidak Ditemukan")));
        }

        return Mono.just(new DtoResponse<>("GET DATA ONU TYPE", Status.SUCCESS, "ONU Type Berhasil Ditemukan", configProperties.getOnuType().get(vendor)));
    }

    @Override
    public Mono<DtoResponse<List<Gpon>>> findByVendor(String vendor) {
        DtoResponse<List<Gpon>> response = new DtoResponse<>("GET DATA GPON", Status.FAILED, "Gagal Menampilkan GPON By Vendor");
        Flux<Gpon> gponFlux;
        if (headers.getRegional().size() > 0 && headers.getWitel().size() > 0) {
            return regionalService.setServerHttpRequest(headers.getRequest())
                    .then(regionalService.findByVendor(vendor)
                            .map(resp -> {
                                List<Gpon> gpons = new ArrayList<>();
                                if (resp.getStatus().equals(Status.SUCCESS)) {
                                    resp.getData().forEach(regional -> {
                                        regional.getWitels().forEach(witel -> {
                                            witel.getStos().forEach(sto -> {
                                                sto.getGpons().forEach(gpon -> {
                                                    gpons.add(gpon);
                                                });
                                            });
                                        });
                                    });
                                    response.setStatus(Status.SUCCESS);
                                    response.setMessage("Berhasil Menampilkan GPON By Vendor");
                                    response.setData(gpons);
                                }
                                return response;
                            }));
        } else if (headers.getWitel().size() > 0) {
            gponFlux = gponRepository.findByWitelIdInAndVendor(headers.getWitel(), Enum.valueOf(Vendor.class, vendor));
        } else {
            gponFlux = gponRepository.findByVendor(Enum.valueOf(Vendor.class, vendor));
        }

        return gponFlux
                .collectList()
                .map(gpons -> {
                    if (gpons.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan GPON By Vendor");
                        response.setData(gpons);
                    }
                    return response;
                });
    }

    @Override
    @Transactional
    public Mono<DtoResponse<Gpon>> create(Gpon gpon) {
        DtoResponse<Gpon> response = new DtoResponse<>("SAVE DATA GPON", Status.FAILED, "Gagal Menyimpan Data GPON");
        return findByIpAddress(gpon.getIpAddress())
                .zipWith(findByHostname(gpon.getHostname()))
                .flatMap(result -> {
                    DtoResponse<Gpon> resp = result.getT1();
                    if (resp.getStatus().equals(Status.SUCCESS)) {
                        response.setMessage("Gagal Simpan, IP GPON Sudah Ada");
                        return Mono.just(response);
                    }

                    if (result.getT2().getStatus().equals(Status.SUCCESS)) {
                        response.setMessage("Gagal Simpan, Hostname GPON Sudah Ada");
                        return Mono.just(response);
                    }

                    Gpon newGpon = new Gpon(gpon);
                    return generateId()
                            .doOnNext(newGpon::setId)
                            .then(gponRepository.save(newGpon)
                                    .flatMap(g -> {
                                        response.setStatus(Status.SUCCESS);
                                        response.setMessage("Berhasil Save Data GPON");
                                        response.setData(g);
                                        return Mono.just(response);
                                    })
                                    .doOnNext(resp1 -> auditService.auditInventory(resp1, headers))
                                    .onErrorResume(e -> {
                                        e.printStackTrace();
                                        if (!"".equals(e.getMessage()) && e.getMessage() != null) {
                                            response.setMessage(e.getMessage());
                                            if (e.getMessage().contains("violates foreign key constraint"))
                                                response.setMessage("Gagal Simpan, STO ID atau NMS ID Tidak Ditemukan");
                                            else if (e.getMessage().contains("duplicate key value violates unique constraint"))
                                                response.setMessage("Gagal Simpan, GPON ID " + newGpon.getId() + " Sudah Terdaftar");
                                        }
                                        response.setData(null);
                                        return Mono.just(response);
                                    }));
                });
    }

    @Override
    @Transactional
    public Mono<DtoResponse<List<Gpon>>> createBatch(List<Gpon> gponList) {
        DtoResponse<List<Gpon>> response = new DtoResponse<>("SAVE BATCH DATA GPON", Status.FAILED, "Gagal Menyimpan Batch Data GPON");
        return Flux.fromIterable(gponList)
                .flatMap(this::checkGpon)
                .collectList()
                .flatMap(respList -> {
                    List<Gpon> gpons = new ArrayList<>();
                    for (int i = 0; i < respList.size(); i++) {
                        DtoResponse resp = respList.get(i);
                        if (resp.getStatus().equals(Status.FAILED)) {
                            response.setStatus(Status.FAILED);
                            response.setMessage(resp.getMessage());
                            return Mono.just(response);
                        }

                        Gpon gpon = mapperUtil.mappingClass(resp.getData(), Gpon.class);
                        Integer getId = Integer.valueOf(gpon.getId()) + i;
                        String id = "0000000000" + getId;
                        gpon.setId(id.substring(id.length() - 10));
                        gpon.setNew(true);
                        gpons.add(gpon);
                    }

                    return gponRepository.saveAll(gpons)
                            .collectList()
                            .map(gponSaveList -> {
                                response.setStatus(Status.SUCCESS);
                                response.setMessage("Berhasil Menyimpan Batch Data GPON");
                                response.setData(gponSaveList);
                                return response;
                            })
                            .doOnNext(resp1 -> auditService.auditInventory(resp1, headers))
                            .onErrorResume(e -> {
                                e.printStackTrace();
                                if (!"".equals(e.getMessage()) && e.getMessage() != null) {
                                    response.setMessage(e.getMessage());
                                    if (e.getMessage().contains("violates foreign key constraint"))
                                        response.setMessage("Gagal Simpan, STO ID atau NMS ID Tidak Ditemukan");
                                    else if (e.getMessage().contains("duplicate key value violates unique constraint"))
                                        response.setMessage("Gagal Simpan, BATCH GPON ID Sudah Terdaftar");
                                }
                                response.setData(null);
                                return Mono.just(response);
                            });
                });
    }

    @Override
    @Transactional
    public Mono<DtoResponse<Gpon>> update(String id, Gpon gpon) {
        DtoResponse<Gpon> response = new DtoResponse<>("UPDATE DATA GPON", Status.FAILED, "Gagal Update Data GPON");
        return findById(id)
                .zipWith(findByIpAddress(gpon.getIpAddress()))
                .zipWith(findByHostname(gpon.getHostname()))
                .flatMap(result -> {
                    DtoResponse<Gpon> resp = result.getT1().getT1();
                    if (resp.getStatus().equals(Status.SUCCESS)) {
                        Gpon oldGpon = mapperUtil.mappingClass(resp.getData(), Gpon.class);
                        response.setOldData(oldGpon);

                        if (!oldGpon.getIpAddress().equals(gpon.getIpAddress())) {
                            if (result.getT1().getT2().getStatus().equals(Status.SUCCESS)) {
                                response.setMessage("IP GPON Sudah Ada");
                                response.setOldData(null);
                                return Mono.just(response);
                            }
                        }

                        if (!oldGpon.getHostname().equals(gpon.getHostname())) {
                            if (result.getT2().getStatus().equals(Status.SUCCESS)) {
                                response.setMessage("Hostname GPON Sudah Ada");
                                response.setOldData(null);
                                return Mono.just(response);
                            }
                        }

                        String stoId = gpon.getStoId();
                        if (("".equals(stoId) || stoId == null) && gpon.getSto() != null) {
                            if (!"".equals(gpon.getSto().getId()) && gpon.getSto().getId() != null) {
                                stoId = gpon.getSto().getId();
                            }
                        }

                        String nmsId = gpon.getNmsId();
                        if (("".equals(nmsId) || nmsId == null) && gpon.getNms() != null) {
                            if (!"".equals(gpon.getNms().getId()) && gpon.getNms().getId() != null) {
                                nmsId = gpon.getNms().getId();
                            }
                        }

                        Gpon newGpon = resp.getData();
                        newGpon.setIpAddress(gpon.getIpAddress());
                        newGpon.setHostname(gpon.getHostname());
                        newGpon.setVlanInet(gpon.getVlanInet());
                        newGpon.setVlanVoice(gpon.getVlanVoice());
                        newGpon.setDefaultUserNms(gpon.isDefaultUserNms());
                        newGpon.setUsername(gpon.getUsername());
                        newGpon.setPassword(gpon.getPassword());
                        newGpon.setUpdateAt(LocalDateTime.now());
                        newGpon.setStoId(stoId);
                        newGpon.setNmsId(nmsId);

                        return gponRepository.save(newGpon)
                                .flatMap(g -> {
                                    response.setStatus(Status.SUCCESS);
                                    response.setMessage("Berhasil Update Data STO");
                                    response.setData(g);
                                    return Mono.just(response);
                                })
                                .doOnNext(resp1 -> auditService.auditInventory(resp1, headers))
                                .onErrorResume(e -> {
                                    e.printStackTrace();
                                    if (!"".equals(e.getMessage()) && e.getMessage() != null) {
                                        response.setMessage(e.getMessage());
                                        if (e.getMessage().contains("violates foreign key constraint"))
                                            response.setMessage("Gagal Simpan, STO ID atau NMS ID Tidak Ditemukan");
                                    }
                                    response.setData(null);
                                    response.setOldData(null);
                                    return Mono.just(response);
                                });
                    }

                    response.setMessage("ID GPON Tidak Ditemukan");
                    return Mono.just(response);
                });
    }

    @Override
    @Transactional
    public Mono<DtoResponse<Gpon>> delete(String id) {
        DtoResponse<Gpon> response = new DtoResponse<>("DELETE DATA GPON", Status.FAILED, "ID GPON Tidak Ditemukan");
        return findById(id)
                .flatMap(resp -> {
                    if (resp.getStatus().equals(Status.SUCCESS)) {
                        Gpon gpon = resp.getData();
                        return gponRepository.delete(gpon)
                                .then(Mono.just(response)
                                        .map(res -> {
                                            res.setStatus(Status.SUCCESS);
                                            res.setMessage("Berhasil Hapus Data GPON " + gpon.getHostname());
                                            res.setData(gpon);
                                            return res;
                                        }))
                                .doOnNext(resp1 -> auditService.auditInventory(resp1, headers))
                                .onErrorResume(e -> {
                                    e.printStackTrace();
                                    response.setMessage(e.getMessage());
                                    response.setData(null);
                                    response.setOldData(null);
                                    return Mono.just(response);
                                });
                    }
                    return Mono.just(response);
                });
    }

    @Override
    public ResponseEntity<Resource> downloadTemplate(String filename) {
        DtoResponse response = new DtoResponse("DOWNLOAD FILE", Status.FAILED, "Could not determine file type.");
        Resource resource = fileStorageService.loadFileAsResource(filename);

        String contentType = null;
        //            contentType = headers.getRequest().getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        response.setStatus(Status.SUCCESS);
        response.setMessage("Berhasil Download File");

        if (contentType == null)
            contentType = "application/vnd.ms-excel";

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    private Mono<String> generateId() {
        return gponRepository.getMaxId()
                .map(getId -> {
                    String id = "0000000000" + getId;
                    id = id.substring(id.length() - 10);
                    return id;
                });
    }

    private Mono<DtoResponse> checkGpon(Gpon gpon) {
        DtoResponse<Gpon> response = new DtoResponse<>("VALIDATE BATCH DATA GPON", Status.FAILED, "Gagal Menyimpan Batch Data GPON");
        return gponRepository.findByHostname(gpon.getHostname())
                .map(g -> {
                    response.setMessage(response.getMessage() + ", Hostname " + gpon.getHostname() + " Sudah Ada");
                    return mapperUtil.mappingClass(response, DtoResponse.class);
                })
                .switchIfEmpty(gponRepository.findByIpAddress(gpon.getIpAddress())
                        .map(g -> {
                            response.setMessage(response.getMessage() + ", IP GPON " + g.getIpAddress() + " Sudah Ada");
                            return mapperUtil.mappingClass(response, DtoResponse.class);
                        })
                        .switchIfEmpty(Mono.just(gpon)
                                .flatMap(g -> {
                                    if (g.getSto() == null) {
                                        response.setMessage(response.getMessage() + ", STO Tidak Boleh Kosong");
                                        return Mono.just(mapperUtil.mappingClass(response, DtoResponse.class));
                                    }
                                    if (g.getSto().getWitel() == null) {
                                        response.setMessage(response.getMessage() + ", Witel Tidak Boleh Kosong");
                                        return Mono.just(mapperUtil.mappingClass(response, DtoResponse.class));
                                    } else if (g.getSto().getWitel().getRegional() == null) {
                                        response.setMessage(response.getMessage() + ", Regional Tidak Boleh Kosong");
                                        return Mono.just(mapperUtil.mappingClass(response, DtoResponse.class));
                                    }

                                    String stoAlias = g.getSto().getAlias();
                                    String witelAlias = g.getSto().getWitel().getAlias();
                                    String regionalId = g.getSto().getWitel().getRegional().getId();
                                    if ("".equals(stoAlias) || stoAlias == null) {
                                        response.setMessage(response.getMessage() + ", STO Alias Tidak Boleh Kosong");
                                        return Mono.just(mapperUtil.mappingClass(response, DtoResponse.class));
                                    } else if ("".equals(witelAlias) || witelAlias == null) {
                                        response.setMessage(response.getMessage() + ", Witel Alias Tidak Boleh Kosong");
                                        return Mono.just(mapperUtil.mappingClass(response, DtoResponse.class));
                                    } else if ("".equals(regionalId) || regionalId == null) {
                                        response.setMessage(response.getMessage() + ", Regional ID Tidak Boleh Kosong");
                                        return Mono.just(mapperUtil.mappingClass(response, DtoResponse.class));
                                    }

                                    return stoService.findByAliasAndWitelAliasAndRegionalId(stoAlias, witelAlias, regionalId)
                                            .flatMap(resp -> {
                                                if (resp.getStatus().equals(Status.FAILED)) {
                                                    response.setMessage(resp.getMessage());
                                                    return Mono.just(mapperUtil.mappingClass(response, DtoResponse.class));
                                                }

                                                if (g.getNms() == null) {
                                                    response.setMessage(response.getMessage() + ", NMS Tidak Boleh Kosong");
                                                    return Mono.just(mapperUtil.mappingClass(response, DtoResponse.class));
                                                } else if ("".equals(g.getNms().getIpServer()) || g.getNms().getIpServer() == null) {
                                                    response.setMessage(response.getMessage() + ", IP NMS Tidak Boleh Kosong");
                                                    return Mono.just(mapperUtil.mappingClass(response, DtoResponse.class));
                                                }

                                                return nmsService.findByIpServer(g.getNms().getIpServer())
                                                        .flatMap(respNms -> {
                                                            if (respNms.getStatus().equals(Status.FAILED)) {
                                                                response.setMessage(respNms.getMessage());
                                                                return Mono.just(mapperUtil.mappingClass(response, DtoResponse.class));
                                                            }
                                                            g.setNew(true);
                                                            g.setCreateAt(LocalDateTime.now());
                                                            g.setSto(null);
                                                            g.setStoId(resp.getData().getId());
                                                            g.setNms(null);
                                                            g.setNmsId(respNms.getData().getId());
                                                            return generateId()
                                                                    .doOnNext(id -> {
                                                                        g.setId(id);
                                                                    })
                                                                    .then(Mono.just(response)
                                                                            .map(gponDtoResponse -> {
                                                                                response.setStatus(Status.SUCCESS);
                                                                                response.setMessage("VALIDASI BATCH GPON BERHASIL");
                                                                                response.setData(mapperUtil.mappingClass(g, Gpon.class));
                                                                                return mapperUtil.mappingClass(response, DtoResponse.class);
                                                                            })
                                                                    );
                                                        });
                                            });
                                })));
    }
}
