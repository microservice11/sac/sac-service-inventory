package id.co.gtx.sacserviceinventory.service.impl;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacmodules.enumz.Vendor;
import id.co.gtx.sacserviceinventory.entity.Regional;
import id.co.gtx.sacserviceinventory.repository.RegionalRepository;
import id.co.gtx.sacserviceinventory.repository.query.RegionalQueryRepository;
import id.co.gtx.sacserviceinventory.service.RegionalService;
import id.co.gtx.sacserviceinventory.service.WitelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.*;

@Slf4j
@Service
public class RegionalServiceImpl extends MasterServiceImpl implements RegionalService {

    private final RegionalRepository regionalRepository;

    private final RegionalQueryRepository regionalQueryRepository;

    private final WitelService witelService;

    private final StreamBridge streamBridge;

    public RegionalServiceImpl(RegionalRepository regionalRepository,
                               RegionalQueryRepository regionalQueryRepository,
                               WitelService witelService,
                               StreamBridge streamBridge) {
        this.regionalRepository = regionalRepository;
        this.regionalQueryRepository = regionalQueryRepository;
        this.witelService = witelService;
        this.streamBridge = streamBridge;
    }

    @Override
    public Mono<DtoResponse<List<Regional>>> findAll() {
        DtoResponse<List<Regional>> response = new DtoResponse<>("GET ALL DATA REGIONAL", Status.FAILED, "Data Regional Kosong");
        return regionalRepository.findAll(Sort.by("id"))
                .collectList()
                .map(regionals -> {
                    if (regionals.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Semua Data Regional");
                        response.setData(regionals);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<Regional>> findById(String id) {
        DtoResponse<Regional> response = new DtoResponse<>("GET DATA REGIONAL", Status.FAILED, "ID Regional Tidak Ditemukan");
        return regionalRepository.findById(id)
                .map(regional -> {
                    response.setStatus(Status.SUCCESS);
                    response.setMessage("Berhasil Menampilkan Data Regional By ID");
                    response.setData(regional);
                    return response;
                })
                .switchIfEmpty(Mono.just(response));
    }

    @Override
    public Mono<DtoResponse<List<Regional>>> findByIdIn(String[] id) {
        DtoResponse<List<Regional>> response = new DtoResponse<>("GET DATA REGIONAL", Status.FAILED, "Regional Tidak Ditemukan By ID : " + Arrays.toString(id));
        return regionalRepository.findByIdIn(Arrays.asList(id))
                .collectList()
                .map(regionals -> {
                    if (regionals.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data Regional By ID : " + Arrays.toString(id));
                        response.setData(regionals);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<List<Regional>>> findByIdInAndWitelIdIn(String[] id, String[] witelId) {
        DtoResponse<List<Regional>> response = new DtoResponse<>("GET DATA REGIONAL", Status.FAILED, "Regional Tidak Ditemukan By ID : " + Arrays.toString(id) + ", Witel ID: " + Arrays.toString(witelId));
        return regionalRepository.findByIdInAndWiteIdIn(Arrays.asList(id), Arrays.asList(witelId))
                .collectList()
                .map(regionals -> {
                    if (regionals.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data Regional By ID : " + Arrays.toString(id) + ", Witel ID: " + Arrays.toString(witelId));
                        response.setData(regionals);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<List<Regional>>> findByVendor(String vendor) {
        DtoResponse<List<Regional>> response = new DtoResponse<>("GET DATA REGIONAL", Status.FAILED, "Data Regional Tidak Ditemukan By Vendor");
        Flux<Regional> regionalFlux;
        if (headers.getRegional().size() > 0 && headers.getWitel().size() > 0) {
            return regionalQueryRepository.findByIdInAndVendor(headers.getRegional(), Enum.valueOf(Vendor.class, vendor))
                    .flatMap(regional -> loadDataWitel(regional, Enum.valueOf(Vendor.class, vendor)))
                    .collectList()
                    .map(regionalList -> {
                        List<Regional> regionals = new ArrayList<>();
                        if (regionalList.size() > 0) {
                            regionalList.forEach(regional -> {
                                boolean isExistWitel = regional.getWitels()
                                        .stream()
                                        .anyMatch(witel -> headers.getWitel()
                                                .stream()
                                                .anyMatch(witelId ->
                                                        witelId.equals(witel.getId()))
                                        );
                                if (isExistWitel)
                                    regionals.add(regional);
                            });
                        }
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data Regional By Vendor");
                        response.setData(regionals);
                        return response;
                    });
        } else if (headers.getRegional().size() > 0) {
            regionalFlux = regionalQueryRepository.findByIdInAndVendor(headers.getRegional(), Enum.valueOf(Vendor.class, vendor));
        } else if (headers.getWitel().size() > 0) {
            regionalFlux = regionalQueryRepository.findByWitelidInAndVendor(headers.getWitel(), Enum.valueOf(Vendor.class, vendor));
        } else {
            regionalFlux = regionalQueryRepository.findByVendor(Enum.valueOf(Vendor.class, vendor));
        }

        return regionalFlux.collectList()
                .map(regionals -> {
                    if (regionals.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data Regional By Vendor");
                        response.setData(regionals);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<List<Regional>>> findByVendor(String vendor, Integer param) {
        if (param != null && param == 1) {
            DtoResponse<List<Regional>> response = new DtoResponse<>("GET DATA REGIONAL", Status.FAILED, "Data Regional Tidak Ditemukan By Vendor");
            Flux<Regional> regionalFlux;
            if (headers.getRegional().size() > 0 && headers.getWitel().size() > 0) {
                regionalFlux = regionalRepository.findByIdInOrWiteIdInAndVendor(headers.getRegional(), headers.getWitel(), Enum.valueOf(Vendor.class, vendor));
            } else if (headers.getRegional().size() > 0) {
                regionalFlux = regionalRepository.findByIdInAndVendor(headers.getRegional(), Enum.valueOf(Vendor.class, vendor));
            } else if (headers.getWitel().size() > 0) {
                regionalFlux = regionalRepository.findByWitelIdInAndVendor(headers.getWitel(), Enum.valueOf(Vendor.class, vendor));
            } else {
                regionalFlux = regionalRepository.findByVendor(Enum.valueOf(Vendor.class, vendor));
            }

            return regionalFlux
                    .collectList()
                    .map(regionals -> {
                        if (regionals.size() > 0) {
                            response.setStatus(Status.SUCCESS);
                            response.setMessage("Berhasil Menampilkan Data Regional By Vendor");
                            response.setData(regionals);
                        }
                        return response;
                    });
        }
        return findByVendor(vendor);
    }

    @Override
    @Transactional
    public Mono<DtoResponse<Regional>> create(Regional regional) {
        DtoResponse<Regional> response = new DtoResponse<>("SAVE DATA REGIONAL", Status.FAILED, "Gagal Menyimpan Data Regional");
        return findById(regional.getId())
                .flatMap(resp -> {
                    if (resp.getStatus().equals(Status.SUCCESS)) {
                        response.setMessage("ID Regional Sudah Ada");
                        return Mono.just(response);
                    }

                    Regional reg = new Regional(regional);
                    return regionalRepository.save(reg)
                            .map(r -> {
                                response.setStatus(Status.SUCCESS);
                                response.setMessage("Berhasil Menyimpan Data Regional");
                                response.setData(r);
                                return response;
                            })
                            .doOnSuccess(resp1 -> auditService.auditInventory(resp1, headers))
                            .onErrorResume(e -> {
                                e.printStackTrace();
                                if (!"".equals(e.getMessage()) && e.getMessage() != null) {
                                    response.setMessage(e.getMessage());
                                    if (e.getMessage().contains("duplicate key value violates unique constraint"))
                                        response.setMessage("Gagal Simpan, Regional ID " + reg.getId() + " Sudah Terdaftar");
                                }
                                response.setData(null);
                                return Mono.just(response);
                            });
                });
    }

    @Override
    @Transactional
    public Mono<DtoResponse<Regional>> update(String id, Regional regional) {
        DtoResponse<Regional> response = new DtoResponse<>("UPDATE DATA REGIONAL", Status.FAILED, "Gagal Update Data Regional");
        return findById(id)
                .flatMap(resp -> {
                    if (resp.getStatus().equals(Status.FAILED)) {
                        response.setMessage("ID Regional Tidak Ditemukan");
                        return Mono.just(response);
                    }

                    Regional oldReg = new Regional();
                    BeanUtils.copyProperties(resp.getData(), oldReg);
                    response.setOldData(oldReg);

                    Regional reg = resp.getData();
                    reg.setNama(regional.getNama());
                    reg.setUpdateAt(LocalDateTime.now());
                    return regionalRepository.save(reg)
                            .map(r -> {
                                response.setStatus(Status.SUCCESS);
                                response.setMessage("Berhasil Update Data Regional");
                                response.setData(r);
                                return response;
                            })
                            .doOnNext(resp1 -> auditService.auditInventory(resp1, headers))
                            .onErrorResume(e -> {
                                e.printStackTrace();
                                response.setMessage(e.getMessage());
                                return Mono.just(response);
                            });
                });
    }

    @Override
    @Transactional
    public Mono<DtoResponse<Regional>> delete(String id) {
        DtoResponse<Regional> response = new DtoResponse<>("DELETE DATA REGIONAL", Status.FAILED, "ID Regional Tidak Ditemukan");
        return findById(id)
                .flatMap(resp -> {
                    if (resp.getStatus().equals(Status.SUCCESS)) {
                        Regional regional = resp.getData();
                        return regionalRepository.delete(regional)
                                .then(Mono.just(response)
                                        .map(res -> {
                                            res.setStatus(Status.SUCCESS);
                                            res.setMessage("Berhasil Hapus Data Regional");
                                            res.setData(regional);

                                            Map<String, String> map = new HashMap<>();
                                            map.put("id", regional.getId());
                                            streamBridge.send("removeData-out-0", map);
                                            return res;
                                        }))
                                .doOnNext(resp1 -> auditService.auditInventory(resp1, headers))
                                .onErrorResume(e -> {
                                    e.printStackTrace();
                                    if (!"".equals(e.getMessage()) && e.getMessage() != null) {
                                        response.setMessage(e.getMessage());
                                        if (e.getMessage().contains("violates foreign key constraint"))
                                            response.setMessage("Gagal Hapus, Regional Sudah Terbinding Pada Witel");
                                    }
                                    return Mono.just(response);
                                });
                    }
                    return Mono.just(response);
                });
    }

    private Mono<Regional> loadDataWitel(final Regional regional, Vendor vendor) {
        return Mono.just(regional)
                .zipWith(witelService.findByIdInAndRegionalIdAndVendor(headers.getWitel(), regional.getId(), vendor))
                .map(result -> {
                    Regional r = result.getT1();
                    if (result.getT2().getStatus().equals(Status.SUCCESS)) {
                        r.setWitels(result.getT2().getData());
                    }
                    return r;
                });
    }
}
