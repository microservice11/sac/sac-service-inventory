package id.co.gtx.sacserviceinventory.service;

import id.co.gtx.sacmodules.dto.DtoLog;
import id.co.gtx.sacmodules.dto.DtoLogDetail;
import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacmodules.dto.StatusCode;
import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacmodules.service.HeadersService;
import id.co.gtx.sacmodules.utils.MapperUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.stereotype.Service;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Slf4j
@Service
public class AuditService {

    private final MapperUtil mapperUtil;
    private final StreamBridge streamBridge;

    public AuditService(MapperUtil mapperUtil,
                        StreamBridge streamBridge) {
        this.mapperUtil = mapperUtil;
        this.streamBridge = streamBridge;
    }

    public Object auditInventory(DtoResponse<?> result, HeadersService headers) {
        if (result.getStatus().equals(Status.SUCCESS)) {
            Map<String, Object> map = mapperUtil.mappingClass(result);
            if (result.getData() instanceof List) {
                List list = (List) result.getData();
                for (int i = 0; i < list.size(); i++) {
                    String tbname = list.get(i).getClass().getAnnotation(Table.class).value();
                    Map<String, Object> dataNew = mapperUtil.mappingClass(((List<?>) result.getData()).get(i));
                    Map<String, Object> dataOld = mapperUtil.mappingClass(((List<?>) result.getOldData()).get(i));
                    if (result.getOldData() == null) {
                        String code = (String) map.get("code");
                        if (code.equals(StatusCode.SUCCESS_CREATE) || !code.split(" ")[0].equals("DELETE")) {
                            createLog(dataNew, tbname, headers);
                        } else {
                            deleteLog(dataNew, tbname, headers);
                        }
                    } else {
                        updateLog(dataNew, dataOld, tbname, headers);
                    }
                }
            } else {
                String tbname = result.getData().getClass().getAnnotation(Table.class).value();
                if (result.getOldData() == null) {
                    String code = (String) map.get("code");
                    if (code.equals(StatusCode.SUCCESS_CREATE) || !code.split(" ")[0].equals("DELETE")) {
                        createLog(mapperUtil.mappingClass(result.getData()), tbname, headers);
                    } else {
                        deleteLog(mapperUtil.mappingClass(result.getData()), tbname, headers);
                    }
                } else {
                    updateLog(mapperUtil.mappingClass(result.getData()), mapperUtil.mappingClass(result.getOldData()), tbname, headers);
                }
            }
        }
        return result;
    }

    private void createLog(Map<String, Object> dataNew, String tbname, HeadersService headers) {
        List<DtoLogDetail> logDetails = new ArrayList<>();
        for (Map.Entry<String, Object> entry : dataNew.entrySet()) {
            if (!(entry.getValue() instanceof Map)
                    || !(entry.getValue() instanceof List)
                    || !(entry.getValue() instanceof Set)) {
                DtoLogDetail logDetail = DtoLogDetail.builder()
                        .colname(entry.getKey())
                        .newValue("" + entry.getValue())
                        .build();
                logDetails.add(logDetail);
            }
        }

        DtoLog dtoLog = DtoLog.builder()
                .operation("INSERT")
                .message("Menambahkan Data " + tbname)
                .tbname(tbname)
                .tbkey("id=" + dataNew.get("id"))
                .username(headers.getUsername())
                .ipClient(headers.getIpClient())
                .createAt(LocalDateTime.now())
                .logsDetails(logDetails)
                .build();

        streamBridge.send("logData-out-0", dtoLog);
    }

    private void updateLog(Map<String, Object> dataNew, Map<String, Object> dataOld, String tbname, HeadersService headers) {
        List<DtoLogDetail> logDetails = new ArrayList<>();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        for (Map.Entry<String, Object> entry : dataOld.entrySet()) {
            String key = entry.getKey();
            Object oldValue = dataOld.get(key);
            Object newValue = dataNew.get(key);

            if (oldValue instanceof LocalDateTime) {
                oldValue = ((LocalDateTime) oldValue).format(dateTimeFormatter);
                if (!oldValue.equals(newValue)) {
                    DtoLogDetail logDetail = DtoLogDetail.builder()
                            .colname(key)
                            .newValue("" + newValue)
                            .oldValue("" + oldValue)
                            .build();
                    logDetails.add(logDetail);
                }
            } else if (oldValue instanceof LocalDate) {
                oldValue = ((LocalDate) oldValue).format(dateFormatter);
                if (!oldValue.equals(newValue)) {
                    DtoLogDetail logDetail = DtoLogDetail.builder()
                            .colname(key)
                            .newValue("" + newValue)
                            .oldValue("" + oldValue)
                            .build();
                    logDetails.add(logDetail);
                }
            } else if (oldValue instanceof Integer) {
                oldValue = ((Integer) oldValue).toString();
                if (!oldValue.equals(newValue)) {
                    DtoLogDetail logDetail = DtoLogDetail.builder()
                            .colname(key)
                            .newValue("" + newValue)
                            .oldValue("" + oldValue)
                            .build();
                    logDetails.add(logDetail);
                }
            } else if (oldValue instanceof Short) {
                oldValue = ((Short) oldValue).toString();
                if (!oldValue.equals(newValue)) {
                    DtoLogDetail logDetail = DtoLogDetail.builder()
                            .colname(key)
                            .newValue("" + newValue)
                            .oldValue("" + oldValue)
                            .build();
                    logDetails.add(logDetail);
                }
            } else {
                if (oldValue != null) {
                    if (!oldValue.equals(newValue)) {
                        DtoLogDetail logDetail = DtoLogDetail.builder()
                                .colname(key)
                                .newValue("" + newValue)
                                .oldValue("" + oldValue)
                                .build();
                        logDetails.add(logDetail);
                    }
                } else if (newValue != null){
                    DtoLogDetail logDetail = DtoLogDetail.builder()
                            .colname(key)
                            .newValue("" + newValue)
                            .build();
                    logDetails.add(logDetail);
                }
            }
        }

        DtoLog dtoLog = DtoLog.builder()
                .operation("UPDATE")
                .message("Update Data " + tbname)
                .tbname(tbname)
                .tbkey("id=" + dataNew.get("id"))
                .username(headers.getUsername())
                .ipClient(headers.getIpClient())
                .createAt(LocalDateTime.now())
                .logsDetails(logDetails)
                .build();

        streamBridge.send("logData-out-0", dtoLog);
    }

    private void deleteLog(Map<String, Object> dataNew, String tbname, HeadersService headers) {
        List<DtoLogDetail> logDetails = new ArrayList<>();
        for (Map.Entry<String, Object> entry : dataNew.entrySet()) {
            if (!(entry.getValue() instanceof Map) && entry.getKey().equals("id")) {
                DtoLogDetail logDetail = DtoLogDetail.builder()
                        .colname(entry.getKey())
                        .newValue("" + entry.getValue())
                        .build();
                logDetails.add(logDetail);
            }
        }

        DtoLogDetail logDetail = DtoLogDetail.builder()
                .colname("create_at")
                .newValue(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS")))
                .build();
        logDetails.add(logDetail);

        DtoLog dtoLog = DtoLog.builder()
                .operation("DELETE")
                .message("Hapus Data " + tbname)
                .tbname(tbname)
                .tbkey("id=" + dataNew.get("id"))
                .username(headers.getUsername())
                .ipClient(headers.getIpClient())
                .createAt(LocalDateTime.now())
                .logsDetails(logDetails)
                .build();
        streamBridge.send("logData-out-0", dtoLog);
    }
}
