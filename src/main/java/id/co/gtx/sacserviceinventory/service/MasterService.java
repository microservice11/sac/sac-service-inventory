package id.co.gtx.sacserviceinventory.service;

import org.springframework.http.server.reactive.ServerHttpRequest;
import reactor.core.publisher.Mono;

public interface MasterService {
    Mono<Void> setServerHttpRequest(ServerHttpRequest request);
}
