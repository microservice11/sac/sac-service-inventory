package id.co.gtx.sacserviceinventory.service;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacserviceinventory.entity.Regional;
import reactor.core.publisher.Mono;

import java.util.List;

public interface RegionalService extends MasterService {
    Mono<DtoResponse<List<Regional>>> findAll();

    Mono<DtoResponse<Regional>> findById(String id);

    Mono<DtoResponse<List<Regional>>> findByIdIn(String[] id);

    Mono<DtoResponse<List<Regional>>> findByIdInAndWitelIdIn(String[] id, String[] witelId);

    Mono<DtoResponse<List<Regional>>> findByVendor(String vendor);

    Mono<DtoResponse<List<Regional>>> findByVendor(String vendor, Integer param);

    Mono<DtoResponse<Regional>> create(Regional regional);

    Mono<DtoResponse<Regional>> update(String id, Regional regional);

    Mono<DtoResponse<Regional>> delete(String id);
}
