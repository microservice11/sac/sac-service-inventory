package id.co.gtx.sacserviceinventory.config.r2dbc.converter;

import id.co.gtx.sacmodules.enumz.Protocol;
import id.co.gtx.sacmodules.enumz.Vendor;
import id.co.gtx.sacserviceinventory.entity.*;
import io.r2dbc.spi.Row;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import java.time.LocalDateTime;

@ReadingConverter
public class GponReadConverter implements Converter<Row, Gpon> {

    @Override
    public Gpon convert(Row row) {
        Regional regional = null;
        try {
            regional = Regional.builder()
                    .id(row.get("r_id", String.class))
                    .nama(row.get("r_nama", String.class))
                    .createAt(row.get("r_create_at", LocalDateTime.class))
                    .updateAt(row.get("r_update_at", LocalDateTime.class))
                    .build();
        } catch (Exception e) {
        }

        Witel witel = null;
        try {
            witel = Witel.builder()
                    .id(row.get("w_id", String.class))
                    .alias(row.get("w_alias", String.class))
                    .nama(row.get("w_nama", String.class))
                    .createAt(row.get("w_create_at", LocalDateTime.class))
                    .updateAt(row.get("w_update_at", LocalDateTime.class))
                    .regional(regional)
                    .build();

            if (regional != null)
                witel.setRegionalId(regional.getId());
        } catch (Exception e) {
        }

        Sto sto = null;
        try {
            sto = Sto.builder()
                    .id(row.get("sto_id", String.class))
                    .alias(row.get("s_alias", String.class))
                    .nama(row.get("s_nama", String.class))
                    .createAt(row.get("s_create_at", LocalDateTime.class))
                    .updateAt(row.get("s_update_at", LocalDateTime.class))
                    .witel(witel)
                    .build();

            if (witel != null)
                sto.setWitelId(witel.getId());
        } catch (Exception e) {

        }

        Nms nms = null;
        try {
            nms = Nms.builder()
                    .id(row.get("nms_id", String.class))
                    .ipServer(row.get("n_ip_server", String.class))
                    .portTl1(row.get("n_port_tl1", Integer.class))
                    .nama(row.get("n_nama", String.class))
                    .vendor(Enum.valueOf(Vendor.class, (String) row.get("n_vendor")))
                    .protocol(Enum.valueOf(Protocol.class, (String) row.get("n_protocol")))
                    .username(row.get("n_username", String.class))
                    .password(row.get("n_password", String.class))
                    .createAt(row.get("n_create_at", LocalDateTime.class))
                    .updateAt(row.get("n_update_at", LocalDateTime.class))
                    .build();
        } catch (Exception e) {

        }

        Gpon gpon = Gpon.builder()
                .id(row.get("id", String.class))
                .ipAddress(row.get("ip_address", String.class))
                .hostname(row.get("hostname", String.class))
                .vlanInet(row.get("vlan_inet", Integer.class))
                .vlanVoice(row.get("vlan_voice", String.class))
                .defaultUserNms(Boolean.TRUE.equals(row.get("default_user_nms", Boolean.class)))
                .username(row.get("username", String.class))
                .password(row.get("password", String.class))
                .createAt(row.get("create_at", LocalDateTime.class))
                .updateAt(row.get("update_at", LocalDateTime.class))
                .stoId(row.get("sto_id", String.class))
                .sto(sto)
                .nmsId(row.get("nms_id", String.class))
                .nms(nms)
                .build();
        return gpon;
    }
}
