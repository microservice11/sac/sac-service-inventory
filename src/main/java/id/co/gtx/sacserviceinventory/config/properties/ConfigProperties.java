package id.co.gtx.sacserviceinventory.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@ConfigurationProperties(prefix = "config")
public class ConfigProperties {
    private Map<String, List<String>> onuType;
}
