package id.co.gtx.sacserviceinventory.config.r2dbc.converter;

import id.co.gtx.sacserviceinventory.entity.Regional;
import id.co.gtx.sacserviceinventory.entity.Witel;
import io.r2dbc.spi.Row;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import java.time.LocalDateTime;

@ReadingConverter
public class WitelReadConverter implements Converter<Row, Witel> {

    @Override
    public Witel convert(Row row) {
        Regional regional = null;
        try {
            regional = Regional.builder()
                    .id(row.get("regional_id", String.class))
                    .nama(row.get("r_nama", String.class))
                    .createAt(row.get("r_create_at", LocalDateTime.class))
                    .updateAt(row.get("r_update_at", LocalDateTime.class))
                    .build();
        } catch (Exception e) {
            e.getMessage();
        }

        Witel witel = Witel.builder()
                .id(row.get("id", String.class))
                .alias(row.get("alias", String.class))
                .nama(row.get("nama", String.class))
                .createAt(row.get("create_at", LocalDateTime.class))
                .updateAt(row.get("update_at", LocalDateTime.class))
                .regionalId(row.get("regional_id", String.class))
                .regional(regional)
                .build();

        return witel;
    }
}
