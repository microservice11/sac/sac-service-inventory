package id.co.gtx.sacserviceinventory.config.r2dbc.converter;

import id.co.gtx.sacserviceinventory.entity.Regional;
import id.co.gtx.sacserviceinventory.entity.Sto;
import id.co.gtx.sacserviceinventory.entity.Witel;
import io.r2dbc.spi.Row;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import java.time.LocalDateTime;

@Slf4j
@ReadingConverter
public class StoReadConverter implements Converter<Row, Sto> {

    @Override
    public Sto convert(Row row) {
        Regional regional = null;
        try {
            regional = Regional.builder()
                    .id(row.get("r_id", String.class))
                    .nama(row.get("r_nama", String.class))
                    .createAt(row.get("r_create_at", LocalDateTime.class))
                    .updateAt(row.get("r_update_at", LocalDateTime.class))
                    .build();
        } catch (Exception e) {
            e.getMessage();
        }

        Witel witel = null;
        try {
            witel = Witel.builder()
                    .id(row.get("witel_id", String.class))
                    .alias(row.get("w_alias", String.class))
                    .nama(row.get("w_nama", String.class))
                    .createAt(row.get("w_create_at", LocalDateTime.class))
                    .updateAt(row.get("w_update_at", LocalDateTime.class))
                    .regional(regional)
                    .build();

            if (regional != null)
                witel.setRegionalId(regional.getId());
        } catch (Exception e) {
            e.getMessage();
        }

        Sto sto = Sto.builder()
                .id(row.get("id", String.class))
                .alias(row.get("alias", String.class))
                .nama(row.get("nama", String.class))
                .createAt(row.get("create_at", LocalDateTime.class))
                .updateAt(row.get("update_at", LocalDateTime.class))
                .witelId(row.get("witel_id", String.class))
                .witel(witel)
                .build();

        return sto;
    }
}
