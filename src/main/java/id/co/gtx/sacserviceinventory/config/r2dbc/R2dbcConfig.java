package id.co.gtx.sacserviceinventory.config.r2dbc;

import id.co.gtx.sacserviceinventory.config.r2dbc.converter.GponReadConverter;
import id.co.gtx.sacserviceinventory.config.r2dbc.converter.StoReadConverter;
import id.co.gtx.sacserviceinventory.config.r2dbc.converter.WitelReadConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.r2dbc.config.EnableR2dbcAuditing;
import org.springframework.data.r2dbc.convert.R2dbcCustomConversions;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableR2dbcAuditing
@EnableR2dbcRepositories(basePackages = {"id.co.gtx.sacserviceinventory.repository"})
public class R2dbcConfig {

    @Bean
    public R2dbcCustomConversions r2dbcCustomConversions() {
        List<Converter<?, ?>> converterList = new ArrayList<>();
        converterList.add(new WitelReadConverter());
        converterList.add(new StoReadConverter());
        converterList.add(new GponReadConverter());
        return new R2dbcCustomConversions(converterList);
    }
}
