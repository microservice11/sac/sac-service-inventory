package id.co.gtx.sacserviceinventory;

import id.co.gtx.sacserviceinventory.config.properties.ConfigProperties;
import id.co.gtx.sacserviceinventory.config.properties.FileStorageProperties;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.ComponentScan;

@RefreshScope
@SpringBootApplication
@ComponentScan(basePackages = {"id.co.gtx"})
@EnableDiscoveryClient
@EnableConfigurationProperties({FileStorageProperties.class, ConfigProperties.class})
@OpenAPIDefinition(info =
    @Info(title = "Inventory Service API",
        version = "${spring.application.version}",
        description = "Documentation Inventory Service API"))
public class SacServiceInventoryApplication {
    static Logger logger = LoggerFactory.getLogger(SacServiceInventoryApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(SacServiceInventoryApplication.class, args);
    }

}
