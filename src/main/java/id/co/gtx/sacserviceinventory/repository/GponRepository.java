package id.co.gtx.sacserviceinventory.repository;

import id.co.gtx.sacmodules.enumz.Vendor;
import id.co.gtx.sacserviceinventory.entity.Gpon;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Repository
public interface GponRepository extends R2dbcRepository<Gpon, String> {

    @Override
    @Query("select g.*, " +
            "   n.id as n_id, " +
            "   n.ip_server as n_ip_server, " +
            "   n.port_tl1 as n_port_tl1, " +
            "   n.nama as n_nama, " +
            "   n.vendor as n_vendor, " +
            "   n.protocol as n_protocol, " +
            "   n.username as n_username, " +
            "   n.password as n_password, " +
            "   n.create_at as n_create_at, " +
            "   n.update_at as n_update_at, " +
            "   s.id as s_id, " +
            "   s.alias as s_alias, " +
            "   s.nama as s_nama, " +
            "   s.create_at as s_create_at, " +
            "   s.update_at as s_update_at, " +
            "   w.id as w_id, " +
            "   w.alias as w_alias, " +
            "   w.nama as w_nama, " +
            "   w.create_at as w_create_at, " +
            "   w.update_at as w_update_at, " +
            "   r.id as r_id, " +
            "   r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at " +
            "from gpon g " +
            "   inner join nms n on n.id = g.nms_id " +
            "   inner join sto s on s.id = g.sto_id " +
            "   inner join witel w on w.id = s.witel_id " +
            "   inner join regional r on r.id = w.regional_id " +
            "order by r.nama, w.nama, s.nama, g.hostname ")
    Flux<Gpon> findAll();

    @Override
    @Query("select g.*, " +
            "   n.id as n_id, " +
            "   n.ip_server as n_ip_server, " +
            "   n.port_tl1 as n_port_tl1, " +
            "   n.nama as n_nama, " +
            "   n.vendor as n_vendor, " +
            "   n.protocol as n_protocol, " +
            "   n.username as n_username, " +
            "   n.password as n_password, " +
            "   n.create_at as n_create_at, " +
            "   n.update_at as n_update_at, " +
            "   s.id as s_id, " +
            "   s.alias as s_alias, " +
            "   s.nama as s_nama, " +
            "   s.create_at as s_create_at, " +
            "   s.update_at as s_update_at, " +
            "   w.id as w_id, " +
            "   w.alias as w_alias, " +
            "   w.nama as w_nama, " +
            "   w.create_at as w_create_at, " +
            "   w.update_at as w_update_at, " +
            "   r.id as r_id, " +
            "   r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at " +
            "from gpon g " +
            "   inner join nms n on n.id = g.nms_id " +
            "   inner join sto s on s.id = g.sto_id " +
            "   inner join witel w on w.id = s.witel_id " +
            "   inner join regional r on r.id = w.regional_id " +
            "where g.id = :id " +
            "order by r.nama, w.nama, s.nama, g.hostname ")
    Mono<Gpon> findById(String id);

    @Query("select g.*, " +
            "   n.id as n_id, " +
            "   n.ip_server as n_ip_server, " +
            "   n.port_tl1 as n_port_tl1, " +
            "   n.nama as n_nama, " +
            "   n.vendor as n_vendor, " +
            "   n.protocol as n_protocol, " +
            "   n.username as n_username, " +
            "   n.password as n_password, " +
            "   n.create_at as n_create_at, " +
            "   n.update_at as n_update_at, " +
            "   s.id as s_id, " +
            "   s.alias as s_alias, " +
            "   s.nama as s_nama, " +
            "   s.create_at as s_create_at, " +
            "   s.update_at as s_update_at, " +
            "   w.id as w_id, " +
            "   w.alias as w_alias, " +
            "   w.nama as w_nama, " +
            "   w.create_at as w_create_at, " +
            "   w.update_at as w_update_at, " +
            "   r.id as r_id, " +
            "   r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at " +
            "from gpon g " +
            "   inner join nms n on n.id = g.nms_id " +
            "   inner join sto s on s.id = g.sto_id " +
            "   inner join witel w on w.id = s.witel_id " +
            "   inner join regional r on r.id = w.regional_id " +
            "where g.ip_address = :ipAddress " +
            "order by r.nama, w.nama, s.nama, g.hostname ")
    Mono<Gpon> findByIpAddress(String ipAddress);

    @Query("select g.*, " +
            "   n.id as n_id, " +
            "   n.ip_server as n_ip_server, " +
            "   n.port_tl1 as n_port_tl1, " +
            "   n.nama as n_nama, " +
            "   n.vendor as n_vendor, " +
            "   n.protocol as n_protocol, " +
            "   n.username as n_username, " +
            "   n.password as n_password, " +
            "   n.create_at as n_create_at, " +
            "   n.update_at as n_update_at, " +
            "   s.id as s_id, " +
            "   s.alias as s_alias, " +
            "   s.nama as s_nama, " +
            "   s.create_at as s_create_at, " +
            "   s.update_at as s_update_at, " +
            "   w.id as w_id, " +
            "   w.alias as w_alias, " +
            "   w.nama as w_nama, " +
            "   w.create_at as w_create_at, " +
            "   w.update_at as w_update_at, " +
            "   r.id as r_id, " +
            "   r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at " +
            "from gpon g " +
            "   inner join nms n on n.id = g.nms_id " +
            "   inner join sto s on s.id = g.sto_id " +
            "   inner join witel w on w.id = s.witel_id " +
            "   inner join regional r on r.id = w.regional_id " +
            "where g.ip_address = :ipAddress " +
            "   and r.id in (:regionalId) " +
            "order by r.nama, w.nama, s.nama, g.hostname ")
    Mono<Gpon> findByIpAddressAndRegionalIdIn(String ipAddress, List<String> regionalId);

    @Query("select g.*, " +
            "   n.id as n_id, " +
            "   n.ip_server as n_ip_server, " +
            "   n.port_tl1 as n_port_tl1, " +
            "   n.nama as n_nama, " +
            "   n.vendor as n_vendor, " +
            "   n.protocol as n_protocol, " +
            "   n.username as n_username, " +
            "   n.password as n_password, " +
            "   n.create_at as n_create_at, " +
            "   n.update_at as n_update_at, " +
            "   s.id as s_id, " +
            "   s.alias as s_alias, " +
            "   s.nama as s_nama, " +
            "   s.create_at as s_create_at, " +
            "   s.update_at as s_update_at, " +
            "   w.id as w_id, " +
            "   w.alias as w_alias, " +
            "   w.nama as w_nama, " +
            "   w.create_at as w_create_at, " +
            "   w.update_at as w_update_at, " +
            "   r.id as r_id, " +
            "   r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at " +
            "from gpon g " +
            "   inner join nms n on n.id = g.nms_id " +
            "   inner join sto s on s.id = g.sto_id " +
            "   inner join witel w on w.id = s.witel_id " +
            "   inner join regional r on r.id = w.regional_id " +
            "where g.ip_address = :ipAddress " +
            "   and w.id in (:witelId) " +
            "order by r.nama, w.nama, s.nama, g.hostname ")
    Mono<Gpon> findByIpAddressAndWitelIdIn(String ipAddress, List<String> witelId);

    List<Gpon> findByOrderByStoWitelRegionalAscStoWitelAscStoAscHostnameAsc();

    @Query("select g.*, " +
            "   n.id as n_id, " +
            "   n.ip_server as n_ip_server, " +
            "   n.port_tl1 as n_port_tl1, " +
            "   n.nama as n_nama, " +
            "   n.vendor as n_vendor, " +
            "   n.protocol as n_protocol, " +
            "   n.username as n_username, " +
            "   n.password as n_password, " +
            "   n.create_at as n_create_at, " +
            "   n.update_at as n_update_at, " +
            "   s.id as s_id, " +
            "   s.alias as s_alias, " +
            "   s.nama as s_nama, " +
            "   s.create_at as s_create_at, " +
            "   s.update_at as s_update_at, " +
            "   w.id as w_id, " +
            "   w.alias as w_alias, " +
            "   w.nama as w_nama, " +
            "   w.create_at as w_create_at, " +
            "   w.update_at as w_update_at, " +
            "   r.id as r_id, " +
            "   r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at " +
            "from gpon g " +
            "   inner join nms n on n.id = g.nms_id " +
            "   inner join sto s on s.id = g.sto_id " +
            "   inner join witel w on w.id = s.witel_id " +
            "   inner join regional r on r.id = w.regional_id " +
            "where g.ip_address in (:ipAddress) " +
            "order by r.nama, w.nama, s.nama, g.hostname ")
    Flux<Gpon> findByIpAddressIn(List<String> ipAddress);

    @Query("select distinct g.* " +
                    "from sto s, nms n, gpon g  " +
                    "where g.sto_id = s.id  " +
                    "   and g.nms_id = n.id " +
                    "   and s.id = :stoId " +
                    "   and n.vendor = :vendor " +
                    "order by g.hostname asc")
    Flux<Gpon> findByStoAndVendor(String stoId, Vendor vendor);

    @Query("select g.*, " +
            "   n.id as n_id, " +
            "   n.ip_server as n_ip_server, " +
            "   n.port_tl1 as n_port_tl1, " +
            "   n.nama as n_nama, " +
            "   n.vendor as n_vendor, " +
            "   n.protocol as n_protocol, " +
            "   n.username as n_username, " +
            "   n.password as n_password, " +
            "   n.create_at as n_create_at, " +
            "   n.update_at as n_update_at, " +
            "   s.id as s_id, " +
            "   s.alias as s_alias, " +
            "   s.nama as s_nama, " +
            "   s.create_at as s_create_at, " +
            "   s.update_at as s_update_at, " +
            "   w.id as w_id, " +
            "   w.alias as w_alias, " +
            "   w.nama as w_nama, " +
            "   w.create_at as w_create_at, " +
            "   w.update_at as w_update_at, " +
            "   r.id as r_id, " +
            "   r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at " +
            "from gpon g " +
            "   inner join nms n on n.id = g.nms_id " +
            "   inner join sto s on s.id = g.sto_id " +
            "   inner join witel w on w.id = s.witel_id " +
            "   inner join regional r on r.id = w.regional_id " +
            "where w.id in (:witelId) " +
            "   and n.vendor = :vendor " +
            "order by r.nama, w.nama, s.nama, g.hostname ")
    Flux<Gpon> findByWitelIdInAndVendor(List<String> witelId, Vendor vendor);

    @Query("select g.*, " +
            "   n.id as n_id, " +
            "   n.ip_server as n_ip_server, " +
            "   n.port_tl1 as n_port_tl1, " +
            "   n.nama as n_nama, " +
            "   n.vendor as n_vendor, " +
            "   n.protocol as n_protocol, " +
            "   n.username as n_username, " +
            "   n.password as n_password, " +
            "   n.create_at as n_create_at, " +
            "   n.update_at as n_update_at, " +
            "   s.id as s_id, " +
            "   s.alias as s_alias, " +
            "   s.nama as s_nama, " +
            "   s.create_at as s_create_at, " +
            "   s.update_at as s_update_at, " +
            "   w.id as w_id, " +
            "   w.alias as w_alias, " +
            "   w.nama as w_nama, " +
            "   w.create_at as w_create_at, " +
            "   w.update_at as w_update_at, " +
            "   r.id as r_id, " +
            "   r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at " +
            "from gpon g " +
            "   inner join nms n on n.id = g.nms_id " +
            "   inner join sto s on s.id = g.sto_id " +
            "   inner join witel w on w.id = s.witel_id " +
            "   inner join regional r on r.id = w.regional_id " +
            "where r.id in (:regionalId) " +
            "   and n.vendor = :vendor " +
            "order by r.nama, w.nama, s.nama, g.hostname ")
    Flux<Gpon> findByRegionalIdInAndVendor(List<String> regionalId, Vendor vendor);

    @Query("select g.*, " +
            "   n.id as n_id, " +
            "   n.ip_server as n_ip_server, " +
            "   n.port_tl1 as n_port_tl1, " +
            "   n.nama as n_nama, " +
            "   n.vendor as n_vendor, " +
            "   n.protocol as n_protocol, " +
            "   n.username as n_username, " +
            "   n.password as n_password, " +
            "   n.create_at as n_create_at, " +
            "   n.update_at as n_update_at, " +
            "   s.id as s_id, " +
            "   s.alias as s_alias, " +
            "   s.nama as s_nama, " +
            "   s.create_at as s_create_at, " +
            "   s.update_at as s_update_at, " +
            "   w.id as w_id, " +
            "   w.alias as w_alias, " +
            "   w.nama as w_nama, " +
            "   w.create_at as w_create_at, " +
            "   w.update_at as w_update_at, " +
            "   r.id as r_id, " +
            "   r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at " +
            "from gpon g " +
            "   inner join nms n on n.id = g.nms_id " +
            "   inner join sto s on s.id = g.sto_id " +
            "   inner join witel w on w.id = s.witel_id " +
            "   inner join regional r on r.id = w.regional_id " +
            "where n.vendor = :vendor " +
            "order by r.nama, w.nama, s.nama, g.hostname ")
    Flux<Gpon> findByVendor(Vendor vendor);

    @Query("select g.*, " +
            "   n.id as n_id, " +
            "   n.ip_server as n_ip_server, " +
            "   n.port_tl1 as n_port_tl1, " +
            "   n.nama as n_nama, " +
            "   n.vendor as n_vendor, " +
            "   n.protocol as n_protocol, " +
            "   n.username as n_username, " +
            "   n.password as n_password, " +
            "   n.create_at as n_create_at, " +
            "   n.update_at as n_update_at, " +
            "   s.id as s_id, " +
            "   s.alias as s_alias, " +
            "   s.nama as s_nama, " +
            "   s.create_at as s_create_at, " +
            "   s.update_at as s_update_at, " +
            "   w.id as w_id, " +
            "   w.alias as w_alias, " +
            "   w.nama as w_nama, " +
            "   w.create_at as w_create_at, " +
            "   w.update_at as w_update_at, " +
            "   r.id as r_id, " +
            "   r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at " +
            "from gpon g " +
            "   inner join nms n on n.id = g.nms_id " +
            "   inner join sto s on s.id = g.sto_id " +
            "   inner join witel w on w.id = s.witel_id " +
            "   inner join regional r on r.id = w.regional_id " +
            "where g.hostname = :hostname " +
            "order by r.nama, w.nama, s.nama, g.hostname ")
    Mono<Gpon> findByHostname(String hostname);

    @Query("select g.*, " +
            "   n.id as n_id, " +
            "   n.ip_server as n_ip_server, " +
            "   n.port_tl1 as n_port_tl1, " +
            "   n.nama as n_nama, " +
            "   n.vendor as n_vendor, " +
            "   n.protocol as n_protocol, " +
            "   n.username as n_username, " +
            "   n.password as n_password, " +
            "   n.create_at as n_create_at, " +
            "   n.update_at as n_update_at, " +
            "   s.id as s_id, " +
            "   s.alias as s_alias, " +
            "   s.nama as s_nama, " +
            "   s.create_at as s_create_at, " +
            "   s.update_at as s_update_at, " +
            "   w.id as w_id, " +
            "   w.alias as w_alias, " +
            "   w.nama as w_nama, " +
            "   w.create_at as w_create_at, " +
            "   w.update_at as w_update_at, " +
            "   r.id as r_id, " +
            "   r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at " +
            "from gpon g " +
            "   inner join nms n on n.id = g.nms_id " +
            "   inner join sto s on s.id = g.sto_id " +
            "   inner join witel w on w.id = s.witel_id " +
            "   inner join regional r on r.id = w.regional_id " +
            "where s.id = :stoId " +
            "order by r.nama, w.nama, s.nama, g.hostname ")
    Flux<Gpon> findByStoId(String stoId);

    @Query("select g.*, " +
            "   n.id as n_id, " +
            "   n.ip_server as n_ip_server, " +
            "   n.port_tl1 as n_port_tl1, " +
            "   n.nama as n_nama, " +
            "   n.vendor as n_vendor, " +
            "   n.protocol as n_protocol, " +
            "   n.username as n_username, " +
            "   n.password as n_password, " +
            "   n.create_at as n_create_at, " +
            "   n.update_at as n_update_at, " +
            "   s.id as s_id, " +
            "   s.alias as s_alias, " +
            "   s.nama as s_nama, " +
            "   s.create_at as s_create_at, " +
            "   s.update_at as s_update_at, " +
            "   w.id as w_id, " +
            "   w.alias as w_alias, " +
            "   w.nama as w_nama, " +
            "   w.create_at as w_create_at, " +
            "   w.update_at as w_update_at, " +
            "   r.id as r_id, " +
            "   r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at " +
            "from gpon g " +
            "   inner join nms n on n.id = g.nms_id " +
            "   inner join sto s on s.id = g.sto_id " +
            "   inner join witel w on w.id = s.witel_id " +
            "   inner join regional r on r.id = w.regional_id " +
            "where n.id = :nmsId " +
            "order by r.nama, w.nama, s.nama, g.hostname ")
    Flux<Gpon> findByNmsId(String nmsId);

    @Query("SELECT CAST(COALESCE(MAX(id), '0') AS INTEGER) + 1 FROM gpon")
    Mono<Integer> getMaxId();
}
