package id.co.gtx.sacserviceinventory.repository;

import id.co.gtx.sacmodules.enumz.Vendor;
import id.co.gtx.sacserviceinventory.entity.Sto;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface StoRepository extends R2dbcRepository<Sto, String> {

    @Override
    @Query("select r.id as r_id, " +
            "   r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at, " +
            "   w.alias as w_alias, " +
            "   w.nama as w_nama, " +
            "   w.create_at as w_create_at, " +
            "   w.update_at as w_update_at, " +
            "   s.*  " +
            "from sto s " +
            "   inner join witel w on w.id = s.witel_id " +
            "   inner join regional r on r.id = w.regional_id " +
            "order by r.nama, w.nama, s.nama ")
    Flux<Sto> findAll();

    @Override
    @Query("select r.id as r_id, " +
            "   r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at, " +
            "   w.alias as w_alias, " +
            "   w.nama as w_nama, " +
            "   w.create_at as w_create_at, " +
            "   w.update_at as w_update_at, " +
            "   s.*  " +
            "from sto s " +
            "   inner join witel w on w.id = s.witel_id " +
            "   inner join regional r on r.id = w.regional_id " +
            "where s.id = :id " +
            "order by r.nama, w.nama, s.nama ")
    Mono<Sto> findById(String id);

    @Query("select r.id as r_id, " +
            "   r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at, " +
            "   w.alias as w_alias, " +
            "   w.nama as w_nama, " +
            "   w.create_at as w_create_at, " +
            "   w.update_at as w_update_at, " +
            "   s.*  " +
            "from sto s " +
            "   inner join witel w on w.id = s.witel_id " +
            "   inner join regional r on r.id = w.regional_id " +
            "where w.id = :witelId " +
            "order by r.nama, w.nama, s.nama ")
    Flux<Sto> findByWitelId(String witelId);

    @Query("select distinct s.*  " +
            "from sto s " +
            "   inner join witel w on w.id = s.witel_id " +
            "   inner join regional r on r.id = w.regional_id " +
            "   inner join gpon g on s.id = g.sto_id" +
            "   inner join nms n on n.id = g.nms_id " +
            "where w.id = :witelId " +
            "   and n.vendor = :vendor " +
            "order by s.nama ")
    Flux<Sto> findByWitelIdAndVendor(String witelId, Vendor vendor);

    @Query("select r.id as r_id, " +
            "   r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at, " +
            "   w.alias as w_alias, " +
            "   w.nama as w_nama, " +
            "   w.create_at as w_create_at, " +
            "   w.update_at as w_update_at, " +
            "   s.*  " +
            "from sto s " +
            "   inner join witel w on w.id = s.witel_id " +
            "   inner join regional r on r.id = w.regional_id " +
            "where s.alias = :alias " +
            "order by r.nama, w.nama, s.nama ")
    Mono<Sto> findByAlias(String alias);

    @Query("SELECT CAST(COALESCE(MAX(id), '0') AS INTEGER) + 1 FROM sto")
    Mono<Integer> getMaxId();

    @Query("select r.id as r_id, " +
            "   r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at, " +
            "   w.alias as w_alias, " +
            "   w.nama as w_nama, " +
            "   w.create_at as w_create_at, " +
            "   w.update_at as w_update_at, " +
            "   s.*  " +
            "from sto s " +
            "   inner join witel w on w.id = s.witel_id " +
            "   inner join regional r on r.id = w.regional_id " +
            "where s.alias = :alias" +
            "   and w.alias = :witelAlias " +
            "   and r.id = :regionalId " +
            "order by r.nama, w.nama, s.nama ")
    Mono<Sto> findByAliasAndWitelAliasAndRegionalId(String alias, String witelAlias, String regionalId);
}
