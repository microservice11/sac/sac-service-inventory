package id.co.gtx.sacserviceinventory.repository.query;

import id.co.gtx.sacmodules.enumz.Protocol;
import id.co.gtx.sacmodules.enumz.Vendor;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public class NmsQueryRepository {

    private final DatabaseClient client;

    public NmsQueryRepository(DatabaseClient client) {
        this.client = client;
    }

    public Flux<Protocol> findDistinctProtocolByVendor(Vendor vendor) {
        String sql = "select distinct protocol from nms where vendor = :vendor";
        return client.sql(sql)
                .bind("vendor", vendor.getValue())
                .map((row, rowMetadata) -> Enum.valueOf(Protocol.class, row.get("protocol", String.class)))
                .all();
    }

    public Flux<Vendor> findDistinctVendor() {
        String sql = "select distinct vendor from nms order by vendor";
        return client.sql(sql)
                .map((row, rowMetadata) -> Enum.valueOf(Vendor.class, row.get("vendor", String.class)))
                .all();
    }
}
