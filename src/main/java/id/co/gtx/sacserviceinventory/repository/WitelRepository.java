package id.co.gtx.sacserviceinventory.repository;

import id.co.gtx.sacmodules.enumz.Vendor;
import id.co.gtx.sacserviceinventory.entity.Witel;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Repository
public interface WitelRepository extends R2dbcRepository<Witel, String> {

    @Override
    @Query("select r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at, " +
            "   w.* " +
            "from witel w  " +
            "   inner join regional r on r.id = w.regional_id " +
            "where r.id = w.regional_id " +
            "order by r.nama, w.alias")
    Flux<Witel> findAll();

    @Override
    @Query("select r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at, " +
            "   w.* " +
            "from witel w  " +
            "   inner join regional r on r.id = w.regional_id " +
            "where r.id = w.regional_id " +
            "   and w.id = :id " +
            "order by r.nama, w.alias")
    Mono<Witel> findById(String id);

    @Query("select r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at, " +
            "   w.* " +
            "from witel w  " +
            "   inner join regional r on r.id = w.regional_id " +
            "where r.id = w.regional_id " +
            "   and r.id = :regionalId " +
            "order by r.nama, w.alias")
    Flux<Witel> findByRegionalIdOrderByAliasAsc(String regionalId);

    @Query("select r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at, " +
            "   w.* " +
            "from witel w  " +
            "   inner join regional r on r.id = w.regional_id " +
            "where r.id = w.regional_id " +
            "   and r.id in (:regionalId) " +
            "order by r.nama, w.alias")
    Flux<Witel> findByRegionalIdInOrderByAliasAsc(List<String> regionalId);

    @Query(
            value = "select distinct w.* " +
                    "from regional r, witel w, sto s, nms n, gpon g  " +
                    "where w.regional_id = r.id  " +
                    "   and s.witel_id = w.id " +
                    "   and g.sto_id = s.id  " +
                    "   and g.nms_id = n.id " +
                    "   and r.id = :regionalId  " +
                    "   and w.id in (:id) " +
                    "   and n.vendor = :vendor " +
                    "order by w.nama"
    )
    Flux<Witel> findByIdInAndRegionalAndVendor(List<String> id, String regionalId, Vendor vendor);

    @Query(
            value = "select distinct w.* " +
                    "from regional r, witel w, sto s, nms n, gpon g  " +
                    "where w.regional_id = r.id  " +
                    "   and s.witel_id = w.id " +
                    "   and g.sto_id = s.id  " +
                    "   and g.nms_id = n.id " +
                    "   and r.id = :regionalId " +
                    "   and n.vendor = :vendor " +
                    "order by w.nama"
    )
    Flux<Witel> findByRegionalAndVendor(String regionalId, Vendor vendor);

    @Query("select r.nama as r_nama, " +
            "   r.create_at as r_create_at, " +
            "   r.update_at as r_update_at, " +
            "   w.* " +
            "from witel w  " +
            "   inner join regional r on r.id = w.regional_id " +
            "where r.id = w.regional_id " +
            "   and w.alias = :alias " +
            "order by r.nama, w.alias")
    Mono<Witel> findByAlias(String alias);

    @Query("select cast(coalesce(max(id), '0') as integer) + 1 from witel")
    Mono<Integer> getMaxId();
}
