package id.co.gtx.sacserviceinventory.repository;

import id.co.gtx.sacserviceinventory.entity.Nms;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface NmsRepository extends R2dbcRepository<Nms, String> {
    Mono<Nms> findByIpServer(String ipServer);

    Mono<Nms> findByNama(String nama);

    @Query("SELECT CAST(COALESCE(MAX(id), '0') AS INTEGER) + 1 FROM nms")
    Mono<Integer> getMaxId();
}
