package id.co.gtx.sacserviceinventory.repository;

import id.co.gtx.sacmodules.enumz.Vendor;
import id.co.gtx.sacserviceinventory.entity.Regional;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.List;

@Repository
public interface RegionalRepository extends R2dbcRepository<Regional, String> {
    @Query("select distinct r.* " +
            "from regional r " +
            "   inner join witel w on r.id = w.regional_id " +
            "   inner join sto s on w.id = s.witel_id " +
            "   inner join gpon g on s.id = g.sto_id " +
            "   inner join nms n on n.id = g.nms_id " +
            "where (r.id in (:id) or w.id in (:witelId)) " +
            "   and n.vendor = :vendor " +
            "order by r.nama")
    Flux<Regional> findByIdInOrWiteIdInAndVendor(List<String> id, List<String> witelId, Vendor vendor);

    @Query("select distinct r.* " +
            "from regional r " +
            "   inner join witel w on r.id = w.regional_id " +
            "   inner join sto s on w.id = s.witel_id " +
            "   inner join gpon g on s.id = g.sto_id " +
            "   inner join nms n on n.id = g.nms_id " +
            "where r.id in (:id) " +
            "order by r.nama")
    Flux<Regional> findByIdIn(List<String> id);

    @Query("select distinct r.* " +
            "from regional r " +
            "   inner join witel w on r.id = w.regional_id " +
            "   inner join sto s on w.id = s.witel_id " +
            "   inner join gpon g on s.id = g.sto_id " +
            "   inner join nms n on n.id = g.nms_id " +
            "where (r.id in (:id) and w.id in (:witelId)) " +
            "order by r.nama")
    Flux<Regional> findByIdInAndWiteIdIn(List<String> id, List<String> witelId);

    @Query("select distinct r.* " +
            "from regional r " +
            "   inner join witel w on r.id = w.regional_id " +
            "   inner join sto s on w.id = s.witel_id " +
            "   inner join gpon g on s.id = g.sto_id " +
            "   inner join nms n on n.id = g.nms_id " +
            "where r.id in (:id) " +
            "   and n.vendor = :vendor " +
            "order by r.nama")
    Flux<Regional> findByIdInAndVendor(List<String> id, Vendor vendor);

    @Query("select distinct r.* " +
            "from regional r " +
            "   inner join witel w on r.id = w.regional_id " +
            "   inner join sto s on w.id = s.witel_id " +
            "   inner join gpon g on s.id = g.sto_id " +
            "   inner join nms n on n.id = g.nms_id " +
            "where w.id in (:witelId) " +
            "   and n.vendor = :vendor " +
            "order by r.nama")
    Flux<Regional> findByWitelIdInAndVendor(List<String> witelId, Vendor vendor);

    @Query("select distinct r.* " +
            "from regional r " +
            "   inner join witel w on r.id = w.regional_id " +
            "   inner join sto s on w.id = s.witel_id " +
            "   inner join gpon g on s.id = g.sto_id " +
            "   inner join nms n on n.id = g.nms_id " +
            "where n.vendor = :vendor " +
            "order by r.nama")
    Flux<Regional> findByVendor(Vendor vendor);
}
