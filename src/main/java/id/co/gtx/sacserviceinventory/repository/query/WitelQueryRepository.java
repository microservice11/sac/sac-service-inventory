package id.co.gtx.sacserviceinventory.repository.query;

import id.co.gtx.sacmodules.enumz.Protocol;
import id.co.gtx.sacmodules.enumz.Vendor;
import id.co.gtx.sacserviceinventory.entity.Gpon;
import id.co.gtx.sacserviceinventory.entity.Nms;
import id.co.gtx.sacserviceinventory.entity.Sto;
import id.co.gtx.sacserviceinventory.entity.Witel;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class WitelQueryRepository {

    private final DatabaseClient client;

    private final String sql = "select " +
            "   n.id as n_id," +
            "   n.ip_server as n_ip_server," +
            "   n.port_tl1 as n_port_tl1, " +
            "   n.nama as n_nama, " +
            "   n.vendor as n_vendor, " +
            "   n.protocol as n_protocol, " +
            "   n.username as n_username, " +
            "   n.password as n_password, " +
            "   n.create_at as n_create_at, " +
            "   n.update_at as n_update_at, " +
            "   g.id as g_id, " +
            "   g.hostname as g_hostname, " +
            "   g.ip_address as g_ip_address, " +
            "   g.vlan_inet as g_vlan_inet, " +
            "   g.vlan_voice as g_vlan_voice, " +
            "   g.default_user_nms as g_default_user_nms, " +
            "   g.username as g_username, " +
            "   g.password as g_password, " +
            "   g.create_at as g_create_at, " +
            "   g.update_at as g_update_at, " +
            "   s.id as s_id, " +
            "   s.alias as s_alias, " +
            "   s.nama as s_nama, " +
            "   s.create_at as s_create_at, " +
            "   s.update_at as s_update_at, " +
            "   w.* " +
            "from witel w " +
            "   inner join sto s on w.id = s.witel_id " +
            "   inner join gpon g on s.id = g.sto_id " +
            "   inner join nms n on n.id = g.nms_id ";

    public WitelQueryRepository(DatabaseClient client) {
        this.client = client;
    }

    private Flux<Witel> mapping(DatabaseClient.GenericExecuteSpec executeSpec) {
        return executeSpec
                .fetch()
                .all()
                .bufferUntilChanged(result -> result.get("s_id"))
                .bufferUntilChanged(result -> result.get(0).get("id"))
                .map(listWitel -> Witel.builder()
                        .id((String) listWitel.get(0).get(0).get("id"))
                        .alias((String) listWitel.get(0).get(0).get("alias"))
                        .nama((String) listWitel.get(0).get(0).get("nama"))
                        .createAt((LocalDateTime) listWitel.get(0).get(0).get("create_at"))
                        .updateAt((LocalDateTime) listWitel.get(0).get(0).get("update_at"))
                        .regionalId((String) listWitel.get(0).get(0).get("regional_id"))
                        .stos(listWitel.stream()
                                .map(listSto -> Sto.builder()
                                        .id((String) listSto.get(0).get("s_id"))
                                        .alias((String) listSto.get(0).get("s_alias"))
                                        .nama((String) listSto.get(0).get("s_nama"))
                                        .createAt((LocalDateTime) listSto.get(0).get("s_create_at"))
                                        .updateAt((LocalDateTime) listSto.get(0).get("s_update_at"))
                                        .witelId((String) listSto.get(0).get("w_id"))
                                        .gpons(listSto.stream()
                                                .map(listGpon -> Gpon.builder()
                                                        .id((String) listGpon.get("g_id"))
                                                        .hostname((String) listGpon.get("g_hostname"))
                                                        .ipAddress((String) listGpon.get("g_ip_address"))
                                                        .vlanInet((Integer) listGpon.get("g_vlan_inet"))
                                                        .vlanVoice((String) listGpon.get("g_vlan_voice"))
                                                        .defaultUserNms((Boolean) listGpon.get("g_default_user_nms"))
                                                        .username((String) listGpon.get("g_username"))
                                                        .password((String) listGpon.get("g_password"))
                                                        .createAt((LocalDateTime) listGpon.get("g_create_at"))
                                                        .updateAt((LocalDateTime) listGpon.get("g_update_at"))
                                                        .stoId((String) listGpon.get("s_id"))
                                                        .nmsId((String) listGpon.get("n_id"))
                                                        .nms(Nms.builder()
                                                                .id((String) listGpon.get("n_id"))
                                                                .ipServer((String) listGpon.get("n_ip_server"))
                                                                .portTl1((Integer) listGpon.get("n_port_tl1"))
                                                                .nama((String) listGpon.get("n_nama"))
                                                                .vendor(Enum.valueOf(Vendor.class, (String) listGpon.get("n_vendor")))
                                                                .protocol(Enum.valueOf(Protocol.class, (String) listGpon.get("n_protocol")))
                                                                .username((String) listGpon.get("n_username"))
                                                                .password((String) listGpon.get("n_password"))
                                                                .createAt((LocalDateTime) listGpon.get("n_create_at"))
                                                                .updateAt((LocalDateTime) listGpon.get("update_at"))
                                                                .build())
                                                        .build())
                                                .collect(Collectors.toList()))
                                        .build())
                                .collect(Collectors.toList()))
                        .build());
    }
    
    public Flux<Witel> findByIdInAndRegionalIdAndVendor(List<String> id, String regionalId, Vendor vendor) {
        String sql = this.sql + " where w.id in (:id) " +
                "   and w.regional_id = :regionalId" +
                "   and n.vendor = :vendor " +
                "order by w.nama, s.alias, g.hostname ";

        DatabaseClient.GenericExecuteSpec executeSpec = client.sql(sql)
                .bind("id", id)
                .bind("regionalId", regionalId)
                .bind("vendor", vendor.getValue());

        return mapping(executeSpec);
    }
}
