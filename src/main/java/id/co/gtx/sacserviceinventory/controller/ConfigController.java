package id.co.gtx.sacserviceinventory.controller;

import id.co.gtx.sacmodules.dto.DtoConfig;
import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacserviceinventory.service.ConfigService;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/inventory/config")
public class ConfigController {

    private final ConfigService configService;

    public ConfigController(ConfigService configService) {
        this.configService = configService;
    }

    @PostMapping("/batch")
    public Mono<DtoResponse<Long>> BatchConfig(@Validated @RequestBody List<DtoConfig> configs, ServerHttpRequest request) {
        return configService.setServerHttpRequest(request).then(configService.batchConfig(configs));
    }

    @PostMapping("/check/unreg/vendor/{vendor}")
    public Mono<DtoResponse<Long>> checkUnreg(@Validated @RequestBody List<DtoConfig> configs, @PathVariable String vendor, ServerHttpRequest request) {
        return configService.setServerHttpRequest(request).then(configService.checkUnreg(configs, vendor));
    }

    @PostMapping("/check/reg/vendor/{vendor}")
    public Mono<DtoResponse<Long>> checkReg(@Validated @RequestBody List<DtoConfig> configs, @PathVariable String vendor, ServerHttpRequest request) {
        return configService.setServerHttpRequest(request).then(configService.checkReg(configs, vendor));
    }

    @PostMapping("/check/service/vendor/{vendor}")
    public Mono<DtoResponse<Long>> checkService(@Validated @RequestBody List<DtoConfig> configs, @PathVariable String vendor, ServerHttpRequest request) {
        return configService.setServerHttpRequest(request).then(configService.checkService(configs, vendor));
    }

    @PostMapping("/create")
    public Mono<DtoResponse<Long>> create(@Validated @RequestBody DtoConfig config, ServerHttpRequest request) {
        return configService.setServerHttpRequest(request).then(configService.create(config));
    }
}
