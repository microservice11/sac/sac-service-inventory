package id.co.gtx.sacserviceinventory.controller;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacserviceinventory.entity.Gpon;
import id.co.gtx.sacserviceinventory.service.GponService;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/inventory/gpon")
public class GponController {

    private final GponService gponService;

    public GponController(GponService gponService) {
        this.gponService = gponService;
    }

    @GetMapping("")
    public Mono<DtoResponse<List<Gpon>>> getAll() {
        return gponService.findAll();
    }

    @GetMapping("/{id}")
    public Mono<DtoResponse<Gpon>> getById(@PathVariable String id) {
        return gponService.findById(id);
    }

    @GetMapping("/sto/{stoId}/vendor/{vendor}")
    public Mono<DtoResponse<List<Gpon>>> getByStoIdAndVendor(@PathVariable String stoId, @PathVariable String vendor) {
        return gponService.findByStoIdAndVendor(stoId, vendor);
    }

    @GetMapping("/ip-gpon/{ipAddress}")
    public Mono<DtoResponse<Gpon>> getByIp(@PathVariable String ipAddress, ServerHttpRequest request) {
        return gponService.setServerHttpRequest(request).then(gponService.findByIpAddress(ipAddress));
    }

    @GetMapping("/hostname/{hostname}")
    public Mono<DtoResponse<Gpon>> getByHostname(@PathVariable String hostname) {
        return gponService.findByHostname(hostname);
    }

    @GetMapping("/ont-type/vendor/{vendor}")
    public Mono<DtoResponse<List<String>>> getOnuTypeByVendor(@PathVariable String vendor) {
        return gponService.findOnuTypeByVendor(vendor);
    }

    @GetMapping("/vendor/{vendor}")
    public Mono<DtoResponse<List<Gpon>>> getByVendor(@PathVariable String vendor, ServerHttpRequest request) {
        return gponService.setServerHttpRequest(request).then(gponService.findByVendor(vendor));
    }

    @GetMapping("/download/template/{filename:.+}")
    public ResponseEntity<Resource> downloadTemplate(@PathVariable String filename) {
        return gponService.downloadTemplate(filename);
    }

    @PostMapping("")
    public Mono<DtoResponse<Gpon>> create(@Validated @RequestBody Gpon gpon, ServerHttpRequest request) {
        return gponService.setServerHttpRequest(request).then(gponService.create(gpon));
    }

    @PostMapping("/batch")
    public Mono<DtoResponse<List<Gpon>>> createBatch(@Validated @RequestBody List<Gpon> gpons, ServerHttpRequest request) {
        return gponService.setServerHttpRequest(request).then(gponService.createBatch(gpons));
    }

    @PutMapping("/{id}")
    public Mono<DtoResponse<Gpon>> update(@PathVariable String id, @Validated @RequestBody Gpon gpon, ServerHttpRequest request) {
        return gponService.setServerHttpRequest(request).then(gponService.update(id, gpon));
    }

    @DeleteMapping("/{id}")
    public Mono<DtoResponse<Gpon>> delete(@PathVariable String id, ServerHttpRequest request) {
        return gponService.setServerHttpRequest(request).then(gponService.delete(id));
    }
}
