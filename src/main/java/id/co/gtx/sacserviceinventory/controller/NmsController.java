package id.co.gtx.sacserviceinventory.controller;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacmodules.enumz.Protocol;
import id.co.gtx.sacmodules.enumz.Vendor;
import id.co.gtx.sacserviceinventory.entity.Nms;
import id.co.gtx.sacserviceinventory.service.NmsService;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/inventory/nms")
public class NmsController {

    private final NmsService nmsService;

    public NmsController(NmsService nmsService) {
        this.nmsService = nmsService;
    }

    @GetMapping("")
    public Mono<DtoResponse<List<Nms>>> getAll() {
        return nmsService.findAll();
    }

    @GetMapping("/{id}")
    public Mono<DtoResponse<Nms>> getById(@PathVariable String id) {
        return nmsService.findById(id);
    }

    @GetMapping("/ip-server/{ipServer}")
    public Mono<DtoResponse<Nms>> getByIpServer(@PathVariable("ipServer") String ipServer) {
        return nmsService.findByIpServer(ipServer);
    }

    @GetMapping("/nama/{nama}")
    public Mono<DtoResponse<Nms>> getByNama(@PathVariable String nama) {
        return nmsService.findByNama(nama);
    }

    @GetMapping("/vendor")
    public Mono<DtoResponse<List<Vendor>>> getVendor() {
        return nmsService.getVendor();
    }

    @GetMapping("/protocol")
    public Mono<DtoResponse<List<Protocol>>> getProtocol() {
        return nmsService.getProtocol();
    }

    @GetMapping("/protocol/{vendor}")
    public Mono<DtoResponse<List<Protocol>>> getMaxProtocolByVendor(@PathVariable String vendor) {
        return nmsService.findMaxProtocolByVendor(vendor);
    }

    @PostMapping("")
    public Mono<DtoResponse<Nms>> create(@RequestBody Nms nms, ServerHttpRequest request) {
        return nmsService.setServerHttpRequest(request).then(nmsService.create(nms));
    }

    @PutMapping("/{id}")
    public Mono<DtoResponse<Nms>> store(@PathVariable String id, @RequestBody Nms nms, ServerHttpRequest request) {
        return nmsService.setServerHttpRequest(request).then(nmsService.update(id, nms));
    }

    @DeleteMapping("/{id}")
    public Mono<DtoResponse<Nms>> delete(@PathVariable String id, ServerHttpRequest request) {
        return nmsService.setServerHttpRequest(request).then(nmsService.delete(id));
    }
}
