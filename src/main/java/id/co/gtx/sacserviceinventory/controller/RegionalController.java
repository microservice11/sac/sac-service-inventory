package id.co.gtx.sacserviceinventory.controller;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacserviceinventory.entity.Regional;
import id.co.gtx.sacserviceinventory.service.RegionalService;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/inventory/regional")
public class RegionalController {

    private final RegionalService regionalService;

    public RegionalController(RegionalService regionalService) {
        this.regionalService = regionalService;
    }

    @GetMapping
    public Mono<DtoResponse<List<Regional>>> getAll() {
        return regionalService.findAll();
    }

    @GetMapping("/{id}")
    public Mono<DtoResponse<Regional>> getById(@PathVariable String id) {
        return regionalService.findById(id);
    }

    @GetMapping("/{id}/witels/{witelId}")
    public Mono<DtoResponse<List<Regional>>> getByIdInAndWitelIdIn(@PathVariable String[] id, @PathVariable String[] witelId) {
        return regionalService.findByIdInAndWitelIdIn(id, witelId);
    }

    @GetMapping("/in/{id}")
    public Mono<DtoResponse<List<Regional>>> getByIdIn(@PathVariable String[] id) {
        return regionalService.findByIdIn(id);
    }

    @GetMapping("/vendor/{vendor}")
    public Mono<DtoResponse<List<Regional>>> getByVendor(@PathVariable String vendor, ServerHttpRequest request) {
        return regionalService.setServerHttpRequest(request).then(regionalService.findByVendor(vendor));
    }

    @GetMapping("/vendor/{vendor}/param/{param}")
    public Mono<DtoResponse<List<Regional>>> getByVendor(@PathVariable String vendor, @PathVariable Integer param, ServerHttpRequest request) {
        return regionalService.setServerHttpRequest(request).then(regionalService.findByVendor(vendor, param));
    }

    @PostMapping
    public Mono<DtoResponse<Regional>> create(@Validated @RequestBody Regional regional, ServerHttpRequest request) {
        return regionalService.setServerHttpRequest(request).then(regionalService.create(regional));
    }

    @PutMapping("/{id}")
    public Mono<DtoResponse<Regional>> update(@PathVariable String id, @RequestBody Regional regional, ServerHttpRequest request) {
        return regionalService.setServerHttpRequest(request).then(regionalService.update(id, regional));
    }

    @DeleteMapping("/{id}")
    public Mono<DtoResponse<Regional>> delete(@PathVariable String id, ServerHttpRequest request) {
        return regionalService.setServerHttpRequest(request).then(regionalService.delete(id));
    }
}
