package id.co.gtx.sacserviceinventory.controller;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacserviceinventory.entity.Sto;
import id.co.gtx.sacserviceinventory.service.StoService;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/inventory/sto")
public class StoController {

    private final StoService stoService;

    public StoController(StoService stoService) {
        this.stoService = stoService;
    }

    @GetMapping("")
    public Mono<DtoResponse<List<Sto>>> getAll() {
        return stoService.findAll();
    }

    @GetMapping("/{id}")
    public Mono<DtoResponse<Sto>> getById(@PathVariable String id) {
        return stoService.findById(id);
    }

    @GetMapping("/alias/{alias}")
    public Mono<DtoResponse<Sto>> getByAlias(@PathVariable String alias) {
        return stoService.findByAlias(alias);
    }

    @GetMapping("/alias/{alias}/witel/{witelAlias}/regional/{regionalId}")
    public Mono<DtoResponse<Sto>> getByAlias(@PathVariable String alias, @PathVariable String witelAlias, @PathVariable String regionalId) {
        return stoService.findByAliasAndWitelAliasAndRegionalId(alias, witelAlias, regionalId);
    }

    @GetMapping("/witel/{id}")
    public Mono<DtoResponse<List<Sto>>> getByWitelId(@PathVariable("id") String witelId) {
        return stoService.findByWitelId(witelId);
    }

    @GetMapping("/witel/{id}/vendor/{vendor}")
    public Mono<DtoResponse<List<Sto>>> getByWitelIdAndVendor(@PathVariable("id") String witelId, @PathVariable String vendor) {
        return stoService.findByWitelIdAndVendor(witelId, vendor);
    }

    @PostMapping("")
    public Mono<DtoResponse<Sto>> create(@RequestBody Sto sto, ServerHttpRequest request) {
        return stoService.setServerHttpRequest(request).then(stoService.create(sto));
    }

    @PutMapping("/{id}")
    public Mono<DtoResponse<Sto>> update(@PathVariable String id, @RequestBody Sto sto, ServerHttpRequest request) {
        return stoService.setServerHttpRequest(request).then(stoService.update(id, sto));
    }

    @DeleteMapping("/{id}")
    public Mono<DtoResponse<Sto>> deleteSto(@PathVariable String id, ServerHttpRequest request) {
        return stoService.setServerHttpRequest(request).then(stoService.delete(id));
    }
}
