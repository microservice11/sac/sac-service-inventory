package id.co.gtx.sacserviceinventory.controller;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacserviceinventory.entity.Witel;
import id.co.gtx.sacserviceinventory.service.WitelService;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/inventory/witel")
public class WitelController {

    private final WitelService witelService;

    public WitelController(WitelService witelService) {
        this.witelService = witelService;
    }

    @GetMapping
    public Mono<DtoResponse<List<Witel>>> getAll() {
        return witelService.findAll();
    }

    @GetMapping("/{id}")
    public Mono<DtoResponse<Witel>> getById(@PathVariable String id) {
        return witelService.findById(id);
    }

    @GetMapping("/regional/{id}")
    public Mono<DtoResponse<List<Witel>>> getByRegionalId(@PathVariable("id") String regionalId) {
        return witelService.findByRegionalId(regionalId);
    }

    @GetMapping("/regionals/{id}")
    public Mono<DtoResponse<List<Witel>>> getByRegionalIdIn(@PathVariable("id") String[] regionalId) {
        return witelService.findByRegionalIdIn(regionalId);
    }

    @GetMapping("/regional/{id}/vendor/{vendor}")
    public Mono<DtoResponse<List<Witel>>> getByRegionalAndVendor(@PathVariable("id") String regionalId, @PathVariable String vendor, ServerHttpRequest request) {
        return witelService.setServerHttpRequest(request).then(witelService.findByRegionalAndVendor(regionalId, vendor));
    }

    @PostMapping("")
    public Mono<DtoResponse<Witel>> create(@RequestBody Witel witel, ServerHttpRequest request) {
        return witelService.setServerHttpRequest(request).then(witelService.create(witel));
    }

    @PutMapping("/{id}")
    public Mono<DtoResponse<Witel>> update(@PathVariable String id, @RequestBody Witel witel, ServerHttpRequest request) {
        return witelService.setServerHttpRequest(request).then(witelService.update(id, witel));
    }

    @DeleteMapping("/{id}")
    public Mono<DtoResponse<Witel>> delete(@PathVariable String id, ServerHttpRequest request) {
        return witelService.setServerHttpRequest(request).then(witelService.delete(id));
    }
}
