package id.co.gtx.sacserviceinventory.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Table("sto")
public class Sto implements Serializable, Persistable<String> {
    private static final long serialVersionUID = 7915906799122406163L;

    @Id
    private String id;

    private String alias;

    private String nama;

    @Column("witel_id")
    @JsonProperty("witel_id")
    private String witelId;

    @Transient
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Witel witel;

    @Transient
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Gpon> gpons;

    @JsonProperty("create_at")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss[.SSS]")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime createAt;

    @JsonProperty("update_at")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss[.SSS]")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime updateAt;

    @Transient
    @JsonIgnore
    private boolean isNew;

    public Sto(Sto sto) {
        this.id = sto.getId();
        this.alias = sto.getAlias();
        this.nama = sto.getNama();
        this.witel = sto.getWitel();
        this.createAt = LocalDateTime.now();
        if (!"".equals(sto.getWitelId()) && sto.getWitelId() != null) {
            this.witelId = sto.getWitelId();
        } else if (sto.getWitel() != null) {
            if (!"".equals(sto.getWitel().getId()) && sto.getWitel().getId() != null) {
                this.witelId = sto.getWitel().getId();
            }
        } else {
            Assert.notNull(sto.witelId, "witel_id is required");
        }
        this.isNew = true;
    }
}
