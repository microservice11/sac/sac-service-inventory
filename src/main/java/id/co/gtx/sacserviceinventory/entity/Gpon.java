package id.co.gtx.sacserviceinventory.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Base64;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Table("gpon")
public class Gpon implements Serializable, Persistable<String> {
    private static final long serialVersionUID = -1253540430471308353L;

    @Id
    private String id;

    @Column("ip_address")
    @JsonProperty("ip_address")
    private String ipAddress;

    private String hostname;

    @Column("vlan_inet")
    @JsonProperty("vlan_inet")
    private Integer vlanInet;

    @Column("vlan_voice")
    @JsonProperty("vlan_voice")
    private String vlanVoice;

    @Column("default_user_nms")
    @JsonProperty("default_user_nms")
    private boolean defaultUserNms;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String username;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String password;

    @Column("sto_id")
    @JsonProperty("sto_id")
    private String stoId;

    @Transient
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Sto sto;

    @Column("nms_id")
    @JsonProperty("nms_id")
    private String nmsId;

    @Transient
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Nms nms;

    @JsonProperty("create_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss[.SSS]")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime createAt;

    @JsonProperty("update_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss[.SSS]")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime updateAt;

    @Transient
    @JsonIgnore
    private boolean isNew;

    public Gpon(Gpon gpon) {
        this.id = gpon.getId();
        this.ipAddress = gpon.getIpAddress();
        this.hostname = gpon.getHostname();
        this.vlanInet = gpon.getVlanInet();
        this.vlanVoice = gpon.getVlanVoice();
        if (!"".equals(gpon.getUsername()) && gpon.getUsername() != null)
            this.username = Base64.getEncoder().encodeToString(gpon.getUsername().getBytes(StandardCharsets.UTF_8));
        if (!"".equals(gpon.getPassword()) && gpon.getPassword() != null)
            this.password = Base64.getEncoder().encodeToString(gpon.getPassword().getBytes(StandardCharsets.UTF_8));
        this.nms = gpon.getNms();

        if (!"".equals(gpon.getStoId()) && gpon.getStoId() != null)
            this.stoId = gpon.getStoId();
        else if (gpon.getSto() != null)
            if (!"".equals(gpon.getSto().getId()) && gpon.getSto().getId() != null)
                this.stoId = gpon.getSto().getId();
        else
            Assert.notNull(gpon.stoId, "sto_id is required");

        if (!"".equals(gpon.getNmsId()) && gpon.getNmsId() != null)
            this.nmsId = gpon.getNmsId();
        else if (gpon.getNms() != null)
            if (!"".equals(gpon.getNms().getId()) && gpon.getNms().getId() != null)
                this.nmsId = gpon.getNms().getId();
        else
            Assert.notNull(gpon.getNmsId(), "nms_id is required");

        this.createAt = LocalDateTime.now();
        this.isNew = true;
    }
}
