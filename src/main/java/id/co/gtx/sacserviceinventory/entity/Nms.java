package id.co.gtx.sacserviceinventory.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.co.gtx.sacmodules.enumz.Protocol;
import id.co.gtx.sacmodules.enumz.Vendor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table("nms")
public class Nms implements Serializable, Persistable<String> {
    private static final long serialVersionUID = 7324557580497881125L;


    @Id
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String id;

    @JsonProperty("ip_server")
    @Column("ip_server")
    private String ipServer;

    @JsonProperty("port_tl1")
    @Column("port_tl1")
    private Integer portTl1;

    private String nama;

    private Vendor vendor;

    private Protocol protocol;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String username;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String password;

    @Transient
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Gpon> gpons;

    @JsonProperty("create_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss[.SSS]")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime createAt;

    @JsonProperty("update_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss[.SSS]")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime updateAt;

    @Transient
    @JsonIgnore
    private boolean isNew;

    public Nms(Nms nms) {
        this.id = nms.getId();
        this.ipServer = nms.getIpServer();
        this.portTl1 = nms.getPortTl1();
        this.nama = nms.getNama();
        this.vendor = nms.getVendor();
        this.protocol = nms.getProtocol();
        if (!"".equals(nms.getUsername()) && nms.getUsername() != null)
            this.username = Base64.getEncoder().encodeToString(nms.getUsername().getBytes(StandardCharsets.UTF_8));
        if (!"".equals(nms.getPassword()) && nms.getPassword() != null)
            this.password = Base64.getEncoder().encodeToString(nms.getPassword().getBytes(StandardCharsets.UTF_8));
        this.createAt = LocalDateTime.now();
        this.isNew = true;
    }
}
