package id.co.gtx.sacserviceinventory.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Table("witel")
public class Witel implements Serializable, Persistable<String> {
    private static final long serialVersionUID = 9178938490968959883L;

    @Id
    private String id;

    private String alias;

    private String nama;

    @Column("regional_id")
    @JsonProperty("regional_id")
    private String regionalId;

    @Transient
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Regional regional;

    @Transient
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Sto> stos;

    @JsonProperty("create_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss[.SSS]")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime createAt;

    @JsonProperty("update_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss[.SSS]")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime updateAt;

    @Transient
    @JsonIgnore
    private boolean isNew;

    public Witel(Witel witel) {
        this.id = witel.getId();
        this.alias = witel.getAlias();
        this.nama = witel.getNama();
        this.createAt = LocalDateTime.now();
        if (!"".equals(witel.getRegionalId()) && witel.getRegionalId() != null) {
            this.regionalId = witel.getRegionalId();
        } else if (witel.getRegional() != null) {
            if (!"".equals(witel.getRegional().getId()) && witel.getRegional().getId() != null) {
                this.regionalId = witel.getRegional().getId();
            }
        } else {
            Assert.notNull(witel.regionalId, "regional_id is required");
        }
        this.isNew = true;
    }
}
