package id.co.gtx.sacserviceinventory.exception;

import lombok.Getter;

public class InventoryException extends RuntimeException{

    @Getter
    private String code;

    public InventoryException(String message, String code) {
        super(message);
        this.code = code;
    }

    public InventoryException(String message, String code, Throwable cause) {
        super(message, cause);
        this.code = code;
    }
}
