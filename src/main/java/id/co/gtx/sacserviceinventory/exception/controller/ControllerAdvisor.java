package id.co.gtx.sacserviceinventory.exception.controller;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacserviceinventory.exception.InventoryException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@Slf4j
@RestControllerAdvice
public class ControllerAdvisor {

    @ExceptionHandler(InventoryException.class)
    public ResponseEntity<DtoResponse> handleNoSuchClientException(InventoryException ex, WebRequest request) {
        ex.printStackTrace();
        DtoResponse response = new DtoResponse(ex.getCode(), Status.FAILED, ex.getMessage());
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
